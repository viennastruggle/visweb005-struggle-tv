export const data = {
  exhibitors: [
    {
      exhibitorId: 1,
      focusGroupId: "23_6",
      name: "Aàdesokan ",
      title: "",
      description:
        "Not only do humans accumulate waste at alarming rates, but its trajectories, once leaving our hands, touch on every aspect of the human condition. For Aàdesokan, trash presents an opportunity to see ourselves reflected in our own excess and excrescence. It also allows us to reimagine other ways of navigating the globe and inhabiting the earth. Unlike human bodies, waste moves freely across all political borders. Aàdesokan asks us to consider: What if waste were treated the same way as migrants? Through the conceptual framework of “Waste Identity” the artist initiates conversations about identity formation through articulations and metaphors of human displacement and waste disposal. Aàdesokan’s research is informed by continuous psychological assessment of the self and one’s surroundings: “I create for revelation and for inciting change. My hope is to build fellowship around provoking visual impulses that interrogate the mind and its endless perceptions. While navigating structures of observation and attention, I generate proposals and thought experiments around different ways of being within spaces undergoing change.” Through writing, photography, and video, Aàdesokan visually maps the many incarnations of our waste identities. During Open Studios this process is turned outward to engage the broader public and bring artistic practice into dialogue with disciplines outside art through discussions of waste management, human displacement, and climate change.",
      link: "https://openstudios2021.janvaneyck.nl/participants/aadesokan",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241581/Avatars/RealGardensAI/4GardenTR00008_z5gecs.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1624211221/Aa%C3%8Cdesokan_Waste_Identity_Bola_Bola_Living_2020_3_spwfjj.jpg",
      layer: "exhibitors",
      positionX: 3337,
      positionY: 1792,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 2,
    },
    {
      exhibitorId: 2,
      focusGroupId: "24_6",
      name: "Aliki van der Kruijs",
      title: "",
      description:
        "For the past year, Aliki van der Kruijs has been making a weekly pilgrimage to the ENCI quarry in Maastricht. Once an important excavation site for marl stone, it has recently become a nature preserve. There is only one available footpath through the quarry. The ritual of making the walk each week reminds her of the continuous back and forth motion of preparing a warp. As van der Kruijs’ year at Jan van Eyck draws to a close, her loom is nearly dressed.\nThrough prolonged sensorial observations and physical experiences, Van der Kruijs gains proximity to landscapes. From them she extracts (in)visible raw materials that generate energy for possible future collaborations. She sees her work as guiding natural processes to reveal something about our environment. In 2012 van der Kruijs  developed a technique called pluviagraphy to capture raindrops in ink on silk. This ongoing work Made by Rain runs parallel to projects investigating how to weave water based on North Sea data and textiles informed by soil strata.\n            Over time van der Kruijs has amassed a trove of textural observations. At the Jan van Eyck, she adds yet a new vocabulary to this tactile database with small scale tests that synthesize thinking with making and dive deeper into the “grain of matter.” Through craftsmanship and collaboration, these can be scaled up to find a place in the world. Outside the studio, her actions go beyond the boundaries of writing paper, a silkscreen frame, or loom. For Open Studios, she reversed the curtains of the auditorium at summer solstice (June 21th), so that the side that faces outward and has been faded by time and sunlight becomes apparent. By repositioning existing elements, she reveals something that has been there all along.\n",
      link:
        "https://openstudios2021.janvaneyck.nl/participants/aliki-van-der-kruijs",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241582/Avatars/RealGardensAI/4GardenTR00001_lj8lew.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2782/aliki-van-der-kruijs-5.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 516,
      positionY: 1619,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 3,
    },
    {
      exhibitorId: 3,
      focusGroupId: "1_7",
      name: "Arvid & Marie",
      title: "",
      description:
        "“We are disciples from beyond, here to free you from your mutinous nature and continuous worries. We are here to lead you along the infinite sea of electrons. We are Omninaut.” Arvid & Marie’s practice assumes many guises and conduits: from a collaboration with pigs to explore non-human musical culture (Pigstrument, 2016) to an effort to support machine rights with the creation of a part robot part bacteria and yeast colony named SAM (2017–2018) with its very own bank account. As their ever-transforming performative alter-ego Omninaut, they pleasantly guide spectators through a multidimensional sonic dystopia of self-optimization and loss of control. In one incarnation they reveal tellingly, “We indeed do not understand the machines that we make.”As part of a continued endeavor to fabricate shared experiences, the artists have made a conscientious shift from being makers of things to also positioning themselves as allies in moments of crisis. Their current work delves deeper into concepts of artificial ecology: both the fluidity of waste and dubious notions of fixing nature. It includes the ruin of a hybrid tree antenna but also a massage chair to reflect on ideas of personal care and extraction. Arvid & Marie playfully test the limits of free will against increasingly pervasive technologies and hegemonic power structures. “We are Omninaut,” they intone. “How delightful can we be?”",
      link: "https://openstudios2021.janvaneyck.nl/participants/arvid-marie",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241582/Avatars/RealGardensAI/4GardenTR00003_efq1gi.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2807/webp_net-resizeimage_2.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 3212,
      positionY: 396,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 4,
    },
    {
      exhibitorId: 4,
      focusGroupId: "26_6",
      name: "Asha Karami",
      title: "",
      description:
        "Asha Karami first published with Godface (2019), a substantial collection of poems. Notably expressive of an individual, unique voice, the work was nominated for De Grote Poëzie prize, the Herman de Coninck prize and the E. du Perron prize. Her poems have been compared to letters, dialogues, and theatre scenes; resolute, terse and honest. She regularly publishes in the journal nY and is associated with the literary foundation Perdu. She is a youth healthcare physician, yoga teacher and a ringside physician at martial art competitions. Together with Johan van Dijke, she creates video poems, of which God is not a sexist was selected for Zebra, the film poetry festival in Berlin. In 2018, she was runner-up at the National Championship Poetry Slam as an anti-performance performer, having won the grand final at Festina Lente the previous year. Asha Karami learnt four languages in the first seven years of her life, and has changed her name three times. She took photography at the Royal Academy of Art, The Hague, and followed writing courses at the Schrijversvakschool, where, aside from poetry, she opted for playwriting and the essay. During her time at the Jan van Eyck Academie, she had conversations with Michel Dijkstra, independent scholar of Eastern philosophy and Western mysticism, initiated by a kōan which appeared as an epigraph in her first collection. She’s also created new work, silk-screens of her drawings and prepared events at Perdu together with participants of the Jan van Eyck. Asha Karami is known for her formidable astuteness, going against the grain.",
      link: "https://openstudios2021.janvaneyck.nl/participants/asha-karami",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241582/Avatars/RealGardensAI/4GardenTR00006_lwucep.png",
      image:
        "https://janvaneyck.nl/site/assets/files/3547/asha_groene_jurkaangepastjpg_768x768.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 1835,
      positionY: 4715,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 5,
    },
    {
      exhibitorId: 5,
      focusGroupId: "1_7",
      name: "Aslı Hatipoğlu",
      title: "",
      description:
        "Aslı Hatipoğlu is an interdisciplinary artist and chef/cook. Using food as a focal point, she investigates interwoven themes of hunger, science, politics, ancestral knowledge, spirituality, and mental health. She curates participatory dinners that tell stories, during which she observes participant reactions that she later transforms into video works or written texts on edible materials.Hatipoğlu situates her practice in the radical degeneration of our relationship with the environment over the last two centuries. Since the industrial revolution, shifts toward more processed foods have interfered with our microbiomes and how we cope with disease. This has been exacerbated by surges in human population, resource consumption, and energy use, accompanied by altered nutrient cycles, fluctuations in oceanic pH, biodiversity loss, and widespread pollution. In addition, the advent of antibiotics has led to antibiotic resistant bacteria that concentrate in soil and are spread across the planet in human waste streams. As a response, Hatipoğlu cultivates beneficial microbiotic colonies that foster more robust ecosystems. She shares these colonies with others as a way to build new communities and bring attention to ecology. By reintroducing long forgotten ancestral insights into molds and fungi, she aims to change attitudes in ways that will reduce future waste and help sustain a healthier planet. In video works, Hatipoğlu reflects on her own psyche to attain higher consciousness of herself and her embodied heritage. Fermentation becomes a metaphor for rethinking time and productivity in a capitalist society. Through fostering symbiotic relationships with living bacteria, Hatipoğlu’s work seeks not only to shift our perspectives of ourselves but also to reclaim food production and encourage food sovereignty in a non-commercial and liberatory way.",
      link: "https://openstudios2021.janvaneyck.nl/participants/asli-burger",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241582/Avatars/RealGardensAI/4GardenTR00005_c1gyl0.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2783/nested_sequence_asli_alone_00_10_44_14_still009-1.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 679,
      positionY: 522,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 6,
    },
    {
      exhibitorId: 6,
      focusGroupId: "3_7",
      name: "Ben Schwartz",
      title: "",
      description:
        "Bootlegging is the illicit distribution or selling of goods, but its definition says little about its role as a powerful cultural phenomenon. The word came to prominence in the 1920s during US prohibition for those smuggling liquor in their boots. Ever since, it has found expression in black markets and the grey zones of language. Over the years, the practice has been claimed as, among other things, an act of homage, anti-capitalist subversion, and artistic dissemination and preservation. In 2018, while a graphic design fellow at Walker Art Center in Minneapolis, Ben Schwartz used bootlegging as a point of departure for his blog UNLICENSED in the museum’s online journal The Gradient. It became a catalyst for a series of conversations with artists and designers to delve into concepts like authenticity, legitimacy, resistance, mistranslation, and power, but also Deadheads, anarchist t-shirts, copyleft licensing, Shakespeare, Bertolt Brecht, knockoff Guccis, Dapper Dan, and Elvis.As Schwartz’s interview project expanded, he also began assimilating bootlegging into his design practice. In Allen Ruppersberg Intellectual Property 1968–2018 and Brian Roettinger & No Age: Graphic Archive 2007–18 (both 2018), Schwarz appropriated and reconfigured visuals from the artists’ archives within the publications. At Jan van Eyck he has continued his conversation series with the intent of assembling them into a book. For Open Studios he offers glimpses into his research as well as extracts from the forthcoming publication, including fragments of dialogue, image interventions, and excerpts of his own writing. If bootlegging is sustained and renewed only by its continued iteration, Schwartz continues to realize new possibilities in production and circulation. ",
      link:
        "https://openstudios2021.janvaneyck.nl/participants/benjamin-schwartz",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241581/Avatars/RealGardensAI/4GardenTR00007_af4dgq.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1623321753/index_t0hfs6.jpg",
      layer: "exhibitors",
      positionX: 4137,
      positionY: 3651,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 7,
    },
    {
      exhibitorId: 7,
      focusGroupId: "23_6",
      name: "Boram Soh",
      title: "",
      description:
        "One summer night, when only her car’s headlights shone in the dark, Boram Soh collided with the already flattened body of a water deer. Looking up, she caught the eyes of a fawn orbiting the corpse. Transfixed by the traumatic memory of meeting the young deer’s gaze, her work has adopted the shining pupils of one and the flat body of the other as the relationship between the sun of age-old mythical narratives and the biodegradable skin of fermented tea leaves. Her experiments evoke an Egyptian mummy’s dry skin, a water deer’s torn skin, and living human skin, as they extend from the biochemistry of plants and animals to hydrogen-based material communities.Soh’s research juxtaposes the mechanics of matter with abstract concepts of light and darkness, life and death, sacrifice and regeneration. She singularly interprets symbols and sign systems, translating them into hieroglyphics. Through projections of material embodiments in an immanent if precarious future, she tracks water deers across South Korea’s capitalist and ecological landscapes from ancient cave paintings to the present day.",
      link: "https://openstudios2021.janvaneyck.nl/participants/boram-soh",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241581/Avatars/RealGardensAI/4GardenTR00009_byh85l.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1624211706/Geometry_of_the_hunter_I_lmr3vl.jpg",
      layer: "exhibitors",
      positionX: 3628,
      positionY: 1203,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 8,
    },
    {
      exhibitorId: 8,
      focusGroupId: "24_6",
      name: "Charlotte Lagro",
      title: "",
      description:
        "Charlotte Lagro takes everyday objects and relieves them of their intended purpose and function to accommodate new possibilities for exchange and desire. She strips them bare, plays with them and transforms them, and makes videos about them, often inviting others to join in the process. In one work, a refrigerator found in an old farmhouse becomes the subject of rumination and humorous debate. Lagro films artists and theorists in front of the appliance, as if before a great work of art, evaluating and expounding on its virtues. Like the other objects she uses, the refrigerator becomes a medium to share stories, sometimes painful or traumatic ones, that touch on the human condition. In another work, uprooted black-and-white traffic signal poles become the stockings of Alice in Wonderland or prison bars. With sleight of hand, Lagro can make heavy objects appear almost weightless.\n         Her current work centers around an umbilical cable used to supply oxygen, as well as light and communication signals, to deep sea divers. From a hundred meters below, divers climb the umbilical, hand over hand, to make their way back to the surface. In water, its air-filled tubes have just the right amount of added mass to gently sink. Out of water, the cable is less at ease. It twists and twirls in all directions. To hold one is to wrestle with it. The 115-meter-long cable is comprised of bright blue, red, orange, and yellow tubes, like veins entwined. (It is a snake of mythical proportions.) This hallowed lifeline makes visceral the fragility of our bodies and the precarity of wedding them to machines. If something goes wrong, it goes terribly wrong.\n",
      link:
        "https://openstudios2021.janvaneyck.nl/participants/charlotte-lagro",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241582/Avatars/RealGardensAI/4GardenTR00004_etsfzb.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2786/webp_net-resizeimage_5.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 4593,
      positionY: 4441,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 9,
    },
    {
      exhibitorId: 9,
      focusGroupId: "26_6",
      name: "David Habets",
      title: "",
      description:
        "Lichens exist only in symbiosis: fungi and algae living in reciprocity, sometimes for thousands of years. They share a peculiar kinship with artist, physicist, and landscape architect David Habets, whose situation-specific works unfold over long and sustained periods of collaboration. Like lichen, they put into relief the brief interval that is human life. Over the last five years, Habets has amassed a non-destructive archive of over 8,000 photos of lichen native to Amsterdam, Maastricht, Vorarlberg, and Chernobyl. In particular he has homed in on one called xanthoria parientina found in and around Maastricht, where it flourishes in landscapes heavily nitrified by industry and agriculture. Xanthoria is recognizable in part by its yellowish-orange hue, which it evolved to protect itself from harsh ultraviolet rays. These lichens are extremophiles: they thrive in niches inhospitable to other living beings. For Habets they have come to signify the increasingly difficult and extreme conditions for living that human lives produce. Like xanthoria, we too live off nitrified landscapes force-fed with artificial fertilizers. As Vroman Fellow at Jan van Eyck, Habets has initiated a 200-year project that uses lichens as marks of our ways of living. We are all extremophiles begins with a temporary installation on the windows of the Jan van Eyck. Layering prints from his lichen archive into agar-agar gelatin layers, Habets transforms the building’s sterile, white façade into something mutable and alive. The light cast through its vitrines creates a sanctuary for thinking of new ways of conceiving changes in time and land.",
      link: "https://openstudios2021.janvaneyck.nl/participants/david-habets",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241582/Avatars/RealGardensAI/4GardenTR00002_ty9l9p.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2788/img_20201016_124104.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 4029,
      positionY: 3198,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 10,
    },
    {
      exhibitorId: 10,
      focusGroupId: "1_7",
      name: "Elisa Caldana",
      title: "",
      description:
        "Just one month before COVID-19 took hold in Italy, the first iteration of Elisa Caldana’s installation Shutterstreet came to an end. On view in the courtyard of Palazzo Carignano in Turin, it was a corridor composed of shutters she salvages from closed down shops across the world. Together they forged a portrait of small businesses struggling to survive in an increasingly globalized economy. Since then, images of shuttered streets have become commonplace. Over the last year, Caldana has collected hundreds of voicemails from shops forced to close by the pandemic. Through shutters and voice mails she traces the obscure topography of a global crisis.Caldana’s latest endeavor draws from a little-known law that grants every Italian city, town, or even village the power to invent names for non-existent streets. These streets provide addresses for unsheltered or nomadic inhabitants so that they may access healthcare and voting rights. It is an infrastructure that replaces homelessness with placelessness. These fictional addresses, which are not included on any map, are also used to open real businesses seeking to evade state control and taxes. An installation titled Monument to Non-existing Streets is dedicated to the ghost geographies that give rise to shadow economies. In it Caldana brings together the names of existing fictional streets to form an antimonument to placelessness. As the first chapter of an ongoing work, Monument to Non-existing Streets and its accompanying text are a provisional mapping of an expansive, yet unrealized atlas charting the failures of capitalist systems.",
      link: "https://openstudios2021.janvaneyck.nl/participants/elisa-caldana",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241581/Avatars/RealGardensAI/4GardenTR00012_uonddn.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2789/shutterstreet-elisacaldana.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 1245,
      positionY: 5521,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 11,
    },
    {
      exhibitorId: 11,
      focusGroupId: "3_7",
      name: "Emilia Tapprest",
      title: "",
      description:
        "Emilia Tapprest (NVISIBLE.STUDIO) explores how value paradigms become embedded in emerging techno-cultural conditions. Arriving from a collaborative design background, she has turned to cinema as a means to engage with the complexity, interconnectedness, and intensity of embodied, lived experience. Drawing a bridge between the two fields, she uses the notion of affective atmospheres to study the material affordances and subjective implications of designed systems.",
      link:
        "https://openstudios2021.janvaneyck.nl/participants/emilia-tapprest",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241580/Avatars/RealGardensAI/4GardenTR00018_umgatj.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1624211344/emilia-gardenimage-72-small_kcqlu1.jpg",
      layer: "exhibitors",
      positionX: 2530,
      positionY: 2768,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 12,
    },
    {
      exhibitorId: 12,
      focusGroupId: "23_6",
      name: "Erika Roux",
      title: "",
      description:
        "In 2017 video artist Erika Roux encountered the then nascent La Révolution est en marche (LREEM), a grassroots organization working toward social justice in Aulnay sous Bois, the Parisian banlieue where she grew up. What began as a request to observe and film them during private gatherings and public actions grew into an intimate bond. The past year she has accompanied the collective to rallies, protests, meetings, and assemblies as they sought to expand what was initiated as a Facebook page into a widespread social movement.LREEM’s members frequently use cellphone cameras to broadcast political objectives, record tense and impassioned street actions, or document the living conditions they seek to change. Roux’s own filmmaking captures many of these self-presentations, as well as generally off-camera, quieter moments of reflection among members in each other’s homes. Her latest video work captures LREEM’s attempts to create a new political imaginary by bringing together underprivileged and under-represented communities nationwide. It documents various collective struggles, including the endeavor to gather signatures from 500 mayors across the country in order to make cofounder Hadama Traoré an eligible candidate in the 2022 presidential elections.  Together with fellow Jan van Eyck participant Rudy Guedj, Roux takes screenshots from LREEM’s online videos and blows them up to poster size. The posters allow the collective’s digital presence to intervene in public space. An archive of the screenshots will also be presented in a book alongside collected testimonies, essays, and documents. Co-published by Building Fictions (Guedj’s publishing imprint) and NoGoZone (LREEM’s), the book will straddle the realms of art publishing, political archiving, and social engagement, while also reflecting on Roux’s multiple entanglements within the movement.",
      link: "https://openstudios2021.janvaneyck.nl/participants/erika-roux",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241581/Avatars/RealGardensAI/4GardenTR00010_m2y2zw.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2791/jvethe_world_has_shrank_final_proxy_mp4_00_04_42_22_still010.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 4797,
      positionY: 943,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 13,
    },
    {
      exhibitorId: 13,
      focusGroupId: "24_6",
      name: "Erin Johnson",
      title: "",
      description:
        "Erin Johnson is a visual artist working primarily in video and video installation. Her research-driven works blend documentary, experimental, and narrative filmmaking devices to foreground the ways in which individual lives and sociopolitical realities merge. Comprised of footage of site-specific performances, the videos explore how power structures are communicated through interpersonal relationships that bring into focus histories of nationalism and place. Johnson is currently working on a series of video works about protest, collectivities, scientific research and imperialist experimentation, and the queer body. In Tomatoes (2020), the camera circles a group of artists as they sit together in a field eating, licking, and squeezing ripe tomatoes. Throughout the ever-changing scene, kisses, whispers, and caresses are shared with a casual, gentle intimacy that reflects interconnectivity and abundance. These queer and desirous exchanges constitute a portrait of collectivity wherein individuals come together as distinct parts of a whole.",
      link: "https://openstudios2021.janvaneyck.nl/participants/erin-johnson",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241581/Avatars/RealGardensAI/4GardenTR00011_filij5.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2792/untitled_project_12_1_1-1.1500x1500.png",
      layer: "exhibitors",
      positionX: 3461,
      positionY: 4896,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 14,
    },
    {
      exhibitorId: 14,
      focusGroupId: "25_6",
      name: "Eva Posas",
      title: "",
      description:
        "Indigenous knowledges are mediated by colonial language and its instruments of power. In a gesture of resistance, Eva Posas has conceived the term phantom languages to describe metalinguistic narratives that are “hidden beyond words and cannot be observed via letters or syntax. Rather they can be felt, grasped, or performed through symbolism, the haptic elements of ritual, or the languages of the body.” At times, they borrow the shapes of objects “fully charged of language beyond their own definition.”\n            One such object is the Xigagueta, a vessel made from the lacquered shell of a calabash and often painted with plants and flowers. These are the same flowers embroidered on huipiles worn by the Zapotec women who shape and nourish others with these gourds. In the indigenous language of Zapotec, Xigagueta is composed of the words xiga, meaning jícara, or vessel, and gueta, meaning tortilla. It is both a container for food and a ritual object for storytelling. For Posas, it is an everyday tool with poetic aesthetics. It is a shelter for imagination.\n            At Jan van Eyck, Posas has adopted the Xigagueta as a temporal space for art, theory, and exhibition making. She uses it as a metaphor to initiate a sustained, inter-institutional dialogue among the academy’s artists and those of the Rijksakademie. In the name of this vessel, they come together to exchange thoughts and ideas about one another’s work. In contrast to the white cube, Posas creates what she describes as “a circular womb full of colors” for encounters and gatherings, ideas and reflections.\nDuring Open Studios, Xigagueta blurs the lines between studio, exhibition space and research site with When they woke up, the dinosaur was still there, which brings together works by Moosje Goosen, Colegio de la Desextinción, Diana Cantarey, and Daniel Aguilar Ruvalcaba. The title derives from a short story by Augusto Monterroso and traces connections between Acámbaro and Chicxulub, Mexico through geofictions that center around extinction, dreams, history, colonialism, phantom structures, and dinosaurs.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/eva-posas",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241579/Avatars/RealGardensAI/4GardenTR00024_q1mpdw.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1623327958/WhatsApp_Image_2021-06-10_at_14.15.29_wwci57.jpg",
      layer: "exhibitors",
      positionX: 1769,
      positionY: 1838,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 15,
    },
    {
      exhibitorId: 15,
      focusGroupId: "23_6",
      name: "Fazal Rizvi",
      title: "",
      description:
        "In the summer of 2020, Fazal Rizvi trekked through the Karakoram mountains in Pakistan hoping to collect sounds of glaciers and the surrounding territory. Hearing the audio files for the first time upon his arrival in Maastricht, he realized they had captured nothing of what his ears had witnessed. He deemed the exercise a failure. As the first part of a long-term and open-ended research project, Rizvi has been working and thinking through this predicament of failure: the failure to record, the failure to listen, the failure even to realize that documenting an unruly landscape aurally could be as difficult as trying to walk it. He reflects too on how his meditations over and navigations through perceived failure surface feelings of desire: Rizvi’s desire to seek refuge in these mountains, to seek their sounds, their crevices, and their stories. They bring him to the crux of his inquiry: What does this desire represent? Where does it rest in regard to the history of the gaze on such places and such terrains? Adopting a mountaineer’s lexicon, Rizvi approaches his studio as a base camp. A base camp is a point of arrival, but also a point of departure. It is a space to store provisions, and a site from which to behold the summit. As Rizvi’s studio, a base camp also becomes a backstage, before which the summit or next campsite unfolds as a play or takes center stage. This space becomes a repository, an archive, or a chapter where each of these journeys—of time and place; human and nonhuman; personal, historical, colonial, and mythical—meet, intersect, and interact.",
      link: "https://openstudios2021.janvaneyck.nl/participants/fazal-rizvi",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241580/Avatars/RealGardensAI/4GardenTR00016_hnwcfm.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1623496382/20210511_125820_jx3xmy.jpg",
      layer: "exhibitors",
      positionX: 2186,
      positionY: 2478,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 16,
    },
    {
      exhibitorId: 16,
      focusGroupId: "",
      name: "Floor Martens",
      title: "",
      description:
        "Floor Martens cuts close to the bone. After discovering the fragile corpse of a rabbit in her parents’ garden, she decided to get as near to life and death as she could touch. She became apprentice to a hunter and leather tanner for a day, and she learned how to butcher meat. This led to experiments using animal blood, as well as her own, making little objects she calls bloodstones. Performing alchemical feats, she also turned menstrual blood and semen into powder. When poured in two circles on the floor alongside one another, the two sources of life suddenly became two deaths, lying side by side. Like the human hair she dreads and braids into hanging sculptural forms, Martens’ source materials are the decomposing remnants of a body. By intervening in the process of decay, or holding onto its traces, she finds a way of also holding onto memory and keeping something alive. Photographing friends and loved ones has become another way for Martens to freeze time. In stills she tries to capture corporeal expressions of their psyches. Photographs showing muscles and bones lithe beneath skin connect the audience with something nonverbal and primal. Whether an animal’s life cut short or a body forever distilled in a frame, Martens’ looks for vestiges of the soul, of the ineffable, in moments of arrested movement.",
      link: "https://openstudios2021.janvaneyck.nl/participants/floor-martens",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241579/Avatars/RealGardensAI/4GardenTR00023_qc4mzu.png",
      image:
        "https://janvaneyck.nl/site/assets/files/3551/vos_polaroid_floor_martens_1.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 922,
      positionY: 4000,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 17,
    },
    {
      exhibitorId: 17,
      focusGroupId: "2_7",
      name: "Frederik Willem Daem",
      title: "",
      description:
        'F.W. Daem is a Belgian writer and artist. He has written two books, was nominated for several literary awards and won de Debuutprijs 2016. The past few years Frederik has frequently journeyed out of literature. He makes music as Prutser, built an installation of a closed café, owned an art magazine called Oogst and made the exhibition Bühne in Het Bos with photographer Catherine Lemblé. At the Open Studios he will be showing a collection of drawings called "Groet. Groeten. Groetjes" based on old tourist information signs.',
      link:
        "https://openstudios2021.janvaneyck.nl/participants/frederik-willem-daem",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241579/Avatars/RealGardensAI/4GardenTR00025_fz7xk1.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1623680316/C08BDAD2-4BE2-4274-AAFC-71CB7479EF89_g4pvck.jpg",
      layer: "exhibitors",
      positionX: 4774,
      positionY: 3118,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 18,
    },
    {
      exhibitorId: 18,
      focusGroupId: "23_6",
      name: "Gamal Fouad",
      title: "",
      description:
        "Gamal Fouad is a writer and, together with Claire Fons, forms the artist duo Felix & Mumford who have exhibited in Berlin and Bologna, and whose work was acquired for the collection of the Centraal Museum Utrecht. His first book Oneindig eiland (‘Infinite island’) was published in 2016, a hilarious novel about a man who’s invited to teach a writing course on a tropical island, even though he’s never been published himself. Fouad evokes the atmosphere on the island with masterly precision, and is deftly comic in telling the story of the course leader and participants. It’s a captivating novel, somewhat reminiscent of The world according to Garp. His second work De voorhuidenverzamelaar (‘The foreskin collector’ Querido, 2018) was nominated for the Biesheuvel award for the best short stories. Gamal draws on his own youth in Alexandria as well as his experiences as an artist in Berlin for his stories. These are symbolic narratives populated by archetypes, where Fouad works highly associatively. As writer in residence, Gamal Fouad made a statement by buying a prefab cabin online and placing it in the garden of the academy, so he could write in it. In front of the cabin, he built a small fire and had long chats with the other artists around it. He wrote a number of uncompromisingly precise texts about these conversations, Op zoek naar de ontbrekende tijd (‘Searching for absent time’), which can be listened to inside the cabin during the Open Studios.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/gamal-fouad",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241580/Avatars/RealGardensAI/4GardenTR00019_uie9z4.png",
      image:
        "https://janvaneyck.nl/site/assets/files/4000/foto_gamal_fouad-2.1500x1500.jpeg",
      layer: "exhibitors",
      positionX: 570,
      positionY: 3402,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 19,
    },
    {
      exhibitorId: 19,
      focusGroupId: "24_6",
      name: "Gerda Blees",
      title: "",
      description:
        "Gerda Blees is a lucid writer, her books are light in tone and yet know profound depths and far-reaching consequences. This applies to her poetry collected in Dwaallichten (‘Fen-fires’ Podium, 2018), her short stories in Aan doodgaan dachten we nog niet (‘We weren’t yet thinking about dying’ 2017) and particularly her acclaimed novel Wij zijn licht (‘We are light’ 2020). With great subtlety, she describes a commune which abstains from solid food, resulting in one of the residents dying. This subtlety gives voice to household objects, to night and day, to the scene of the incident and to bystanders who all speak as witnesses, as well as the consequences for the enlightened commune. Gerda Blees writes as a visual artist, she allows the components in her work to tell the story. The novel is nominated for the Libris literature prize. At the academy she worked with Boram Soh on an installation and, under the working title De levenden (‘The living’), wrote texts about the aftermath of the Bijlmer disaster in Amsterdam, and its impact on families who lived near the site where the Israeli cargo plane crashed headlong into a block of flats. Blees works on the borderline between reality and fiction. In her new work, a father goes searching for the last thoughts of the victims of the disaster with a special recording device. During the Open Studios, Gerda Blees will present Onderhandelingen met een geest, with an English translation by Michele Hutchinson (‘Talking With The Dead’), a publication by the Jan van Eyck Academie which she herself has designed and printed.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/gerda-blees",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241580/Avatars/RealGardensAI/4GardenTR00017_hnowdo.png",
      image: "",
      layer: "exhibitors",
      positionX: 1532,
      positionY: 1222,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 20,
    },
    {
      exhibitorId: 20,
      focusGroupId: "25_6",
      name: "Giuditta Vendrame",
      title: "",
      description:
        "Giuditta Vendrame interweaves art and design through legal infrastructures with works that focus on personhood, space, and mobility. She combines research on the circulation of bodies in public space with performative moments and symbolic gestures. Through investigations into citizenship and its paradoxes she finds ways to open political spaces through playful and poetic interventions. For several years, Vendrame has been looking at interconnected bodies of water to challenge modern conceptions of nationhood, territory, and borderlands. In contrast to what historian Marcus Rediker terms the Western, “terracentric” mode of looking at the world from a national, bounded, and territorial viewpoint, she seeks to understand the earth from a “humid” perspective. Using oceans and waterways, she proposes new possible world orders that are less stable, ever changing, and circular. She is currently expanding a research project on seafloor spreading and the unique ecologies that take root in places where tectonic plates have split apart. These fluid geological margins provide a conceptual framework to unsettle deeply entrenched notions of political borders. Like the philosopher Emanuele Coccia, Vendrame sees migration as a planetary condition, shared among all beings, who drift along with the continents.",
      link:
        "https://openstudios2021.janvaneyck.nl/participants/giuditta-vendrame",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241579/Avatars/RealGardensAI/4GardenTR00027_tbgegg.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2795/km1_3188.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 2146,
      positionY: 3598,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 21,
    },
    {
      exhibitorId: 21,
      focusGroupId: "24_6",
      name: "Hamja Ahsan",
      title: "",
      description:
        "Hamja Ahsan operates in the fluid space of artist, writer, curator, and activist for histories of British state crime, contemporary Islamophobia, prison abolition, and repression of civil liberties during the War on Terror. His book Shy Radicals: Antisystemic Politics of the Militant Introvert (Italian translation retitled Introfada) lies at the heart of his practice. It is a satirical, speculative fiction envisioning a revolutionary political movement for shy, quiet, awkward, and autistic people against a neurotypical extrovert supremacist world order. The book has been widely adopted in neurodiversity curricula, and Ahsan himself continues to explore its use to transform institutions, employment practices, and government structures. He also serves on the editorial board of Radical Mental Health zine Asylum, a forum to rethink pathology, power, and clinical oppression in relation to racism and imperialism.An ongoing research project is titled 27/72: Reorientating the 27 Club. During his residency, Ahsan has run the 27 Book Club, which reexamines myths surrounding the early deaths of Kurt Cobain, Bobby Sands, and Janis Joplin in relation to the agency and potentiality of romantic self-destruction. Seventy-two refers to the romantic self-preservation of decadent rock stars such as Iggy Pop and their survival of excesses.The Dutch premiere of the film Shy Radicals, made in collaboration with Ridley Scott’s associate director Tom Dream, debuts at Open Studios. Ahsan adopts the role of commander-in-chief of the decolonial introvert movement, on a world tour that will not relent until the extrovert-normative world order is vanquished everywhere. ",
      link: "https://openstudios2021.janvaneyck.nl/participants/hamja-ahsan",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241580/Avatars/RealGardensAI/4GardenTR00020_zi0zrw.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2796/shy-radicals_signs.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 4798,
      positionY: 2624,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 22,
    },
    {
      exhibitorId: 22,
      focusGroupId: "2_7",
      name: "Ignace Cami",
      title: "",
      description:
        "It has been several years since Ignace Cami studied printmaking. As his practice evolved, his interest in printing faded, though a pleasure in the drips, slips, and stutters that arise between impressions persists. Today Cami is an initiator of rumors, fables, and urban legends. He does not so much construct stories as release them. Like folklore, they do not reside in a single orator but in their many recantations. \nOften Cami uses objects to summon his narratives. Baked cookies, but also drinking glasses chained to ceramic goose heads, spinning tops, crumpled-up paper, small sachets of honey, and ceramic rubbing plates, evoke tender stories full of humor. As Cami writes, “O stories, they spread like a virus looking for symbiosis. A host in which they can hibernate, even if it’s for a short while. There they wait to be transformed. They can carry whatever their host injects into them before rereleasing them into the world. This is metamorphosis. A new generation of the story goes viral, carried by the wind of our words, landing on whomever wants to hear it. Ready to go through the cycle once more.” Like his cookies, Cami’s tales are ingested and metabolized by those who, in the process of retelling them, adapt the stories to make them their own. Each voice confers new meaning. From language’s drips, slips, and stutters emerge new possibilities for shared experience.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/ignace-cami",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241580/Avatars/RealGardensAI/4GardenTR00013_iwpdmy.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2798/webp_net-resizeimage_3.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 5279,
      positionY: 3644,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 23,
    },
    {
      exhibitorId: 23,
      focusGroupId: "3_7",
      name: "Inge Schilperoord",
      title: "",
      description:
        "Inge Schilperoord is a writer and a forensic psychologist. In her acclaimed first novel Muidhond (Tench) (2015), she crawled into the skin of a sex offender. She depicts the main character, Jonathan, with such detailed attention to his anxieties and feelings, that she is able to create a deeply convincing protagonist. The novel won the Bronzen Uil prize, and was shortlisted for many other literary awards such as the ECI, the Libris, the Fitro, Opzij, the ANV debut and the Anton Wachter prizes. The novel has been translated into a number of languages including Turkish and Catalan, while the film adaptation of Muidhond, in collaboration with the author herself, was released in 2019. Directed by Patrice Toy, the film is noted for its strong acting and unusual cinematography. The narrative voice was replaced to an important extent with body language. The location is a regeneration district near the coast and, much like the fish of the title, that too plays a fundamental role in the story. Muidhond interrogates a controversial subject that has been addressed before, but not with as much insight and empathy as in the work of Schilperoord. At the Jan van Eyck Academie, Inge Schilperoord worked steadily on her long awaited second novel, which is due to be published in September 2021. She also used her time at the academy to create new material in exchange with the participants. To conclude her residency, she discussed her practice with writer Adriaan van Dis in a public talk.",
      link:
        "https://openstudios2021.janvaneyck.nl/participants/inge-schilperoord",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241580/Avatars/RealGardensAI/4GardenTR00014_rz5s5i.png",
      image:
        "https://janvaneyck.nl/site/assets/files/5403/inge_schilperoord_5572_c_keke_keukelaar_voor_de_morgen_jpg-1.1500x1500.jpeg",
      layer: "exhibitors",
      positionX: 1869,
      positionY: 873,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 24,
    },
    {
      exhibitorId: 24,
      focusGroupId: "23_6",
      name: "Jakub Samek",
      title: "",
      description:
        "Times New Roman is so ubiquitous that its appearance on the page has become unremarkable. Five years ago, Jakub Samek set out to jolt readers’ complacency by redesigning the typeface to “disturb the routine process of reading” and reawaken “the eye’s interest in what it actually sees.” A first sketch combined reassuring alphabet proportions with subtle, disharmonious shifts in soft and sharp components. The draft was never completed, but it remained an important frame of reference.\nSamek ultimately chose a more traditional approach toward his redesign. The resulting typeface, called Rhymes, was received to immediate praise and versions of it were commissioned by the likes of Nike, Frieze, and Jan van Eyck Academie. As its name suggests, Rhymes is more a riff on Times New Roman than a repudiation. Samek succeeded in shaping something complex, lyrical, and wholly original, but not entirely new. He had changed the key, but the melody remained the same.\nIn a new type specimen, Samek reconsiders the roots and evolution of Rhymes. Type specimens provide detailed technical information about a particular font, along with visually immersive examples. Samek’s own book includes poetry and personal reflections, as well as a critical essay by Persis Bekkering situating the typeface in a broader political context. A supplement to the type specimen is dedicated to the bespoke version for the Jan van Eyck that belongs to the new graphic design identity of the academy.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/jakub-samek",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241580/Avatars/RealGardensAI/4GardenTR00015_q6qtxi.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1623519859/WhatsApp_Image_2021-06-12_at_18.31.03_ca2lja.jpg",
      layer: "exhibitors",
      positionX: 543,
      positionY: 2574,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 25,
    },
    {
      exhibitorId: 25,
      focusGroupId: "26_6",
      name: "Jente Posthuma",
      title: "",
      description:
        "Jente Posthuma studied Literary Studies, then worked as journalist, conducting long form interviews for De Groene Amsterdammer, nrc.next en De Volkskrant. Her first work as a literary writer was published in 2016: Mensen zonder uitstraling (‘People without Charisma’, Atlas Contact). That same year with her husband, the photographer Bas Uterwijk, she created a small photo book Probeer een beetje goed over me te denken, (‘Try to think a little well of me’) about the chosen death of Uterwijk’s father. In 2020, her second novel Waar ik liever niet aan denk (‘What I’d rather not think about’) was published by Pluim. Posthuma’s work has been nominated for the Dioraphte Literature prize, de ANV Debutanten prize and the European Union Prize for Literature. During her residency at the Jan van Eyck Academie, Jente Posthuma has been working on an autobiographical work of creative non-fiction about her father, her mother, her own motherhood, her authorship and the way in which many women learn early in life not to express themselves, about the fear to let one’s voice be heard. ‘I don’t want to write a feeble book. I want to do what I find most difficult: speak for myself, be vulnerable,’ as Posthuma describes her way of working. The book is accompanied by a podcast about anger. Reflective voice-overs about religion, war and conflict follow on from narrative fragments, full of humour, but also vulnerable and confrontational. Jente Posthuma writes unadorned, funny, and direct. She does not avoid difficult subjects such as anxiety, annoyances and painful memories in her family: the experiences in a Japanese POW camp and the social environment of Jehovah’s Witnesses. \n",
      link: "https://openstudios2021.janvaneyck.nl/participants/jente-posthuma",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241579/Avatars/RealGardensAI/4GardenTR00022_htfott.png",
      image:
        "https://janvaneyck.nl/site/assets/files/5813/bu188770_omslag-3-1.1500x1500.jpeg",
      layer: "exhibitors",
      positionX: 2682,
      positionY: 2442,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 26,
    },
    {
      exhibitorId: 26,
      focusGroupId: "2_7",
      name: "Johanna Bruckner",
      title: "",
      description:
        "Atmospheric escape is a phenomenon by which molecules cross the Earth’s atmosphere into outer space, where they can potentially merge and create new life forms. Johanna Bruckner’s video works and performances inhabit a similar liminal realm, transgressing the outermost reaches of language to find new possibilities for human and posthuman affective relationships. Her poems and texts reoccupy scientific language to queer the rhetoric of space colonialization. Bruckner seeks and finds resistance within micromolecular and cosmic formations. \nIn video works like Molecular Sex (2020), about a sexbot who mutates freely from one state to another, constellations of moving and falling bodies forge new spaces of indeterminacy. Animation and filmed performances are layered over one another in algorithmic and mechanical processes that allow new aesthetic entanglements and self-performativities to emerge. \nAt Jan van Eyck, Bruckner’s latest work is a collage of Dark Room Sci-Fi, queer post-pornography, and extra-terrestrial social bonding. Bodies are reconceived as affecting multiplicities that allow for more nuanced, queer, and speculative perspectives on the interactions between physical and more-than-human entities. Bruckner pushes the limits of the human sensorium. She invents what she terms “xeno-technological prostheses” that allow us to reimagine and redistribute the relationships and patterns by which subjects comprehend their world.\n\n",
      link:
        "https://openstudios2021.janvaneyck.nl/participants/johanna-bruckner",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241580/Avatars/RealGardensAI/4GardenTR00021_zeharz.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1623686967/Sequenz_03.00_00_24_27.Standbild015_u687mp.jpg",
      layer: "exhibitors",
      positionX: 265,
      positionY: 4369,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 27,
    },
    {
      exhibitorId: 27,
      focusGroupId: "3_7",
      name: "Kanthy Peng",
      title: "",
      description:
        "Through still and moving images, Kanthy Peng explores where tales of women’s bodies in East-Asian folklore intertwine with geographical landscapes. The at times subtle shifts these stories undergo—as, over generations, elements are elaborated upon, altered, or erased—reflect changing attitudes and social, political, and technological transformations. Kanthy appropriates these folktales not only to reveal how they reinforce particular ideas about gender expression and the environment but also as a coded language to broach otherwise taboo subjects of female agency and sexuality.\nIn the tale of Lady Meng Jiangnu, a young woman learns that her husband has died in service building the Great Wall of China. She makes a pilgrimage to the site, where she weeps so bitterly that her tears collapse a portion of the wall, revealing her husband's entombed bones. In unutterable grief, she throws herself into the river. At Open Studios, Kanthy sculpts this story from a non-narrative approach: one in which Meng Jiangnu’s tears become the tale’s protagonist. Through a series of chemical reactions, she condenses artificial tears into semi solid building blocks. Before her edifice can be fully erected, however, the bricks’ fragile casings begin to weaken. They sag and leak under their own weight, until at last, the entire wall gives way.\nTwo other works on view, the animated video View from the Window at Le Gras and the print series Tagged Museum Garden, explore the act of gazing at or into landscapes, specifically gardens.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/kanthy-peng",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241579/Avatars/RealGardensAI/4GardenTR00026_ghwivj.png",
      image: "https://janvaneyck.nl/site/assets/files/2802/11.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 963,
      positionY: 4441,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 28,
    },
    {
      exhibitorId: 28,
      focusGroupId: "24_6",
      name: "Lisa Ertel & Jannis Zell",
      title: "Sense of Place",
      description:
        "\rLisa Ertel and Jannis Zell are designers and artists who visualize thoughts around matter and time as well as nature and culture. Together they materialize the veiled histories, modes of representation, and conditioning of everyday objects through artistic and design-based approximation.\r\rAlong recurring walks in urban and rural landscapes, they try to suss out the pervading spirit of a particular place—its genius loci—from which they test out rituals of creating, informed by its actants and its mysteries. Together they collect nonhuman artifacts, or biofacts, to bring back to their studio. These objects and traces become part of a growing Wunderkammer that the designers plumb for associative shapes and images.\r\rSlowly the collected souvenirs blend into the studio’s interior, and a dialogue between inside and outside emerges. Raw materials become tame or assume domestic functions as they adapt to their new surroundings, while tools and objects begin to seem wild or animate. Through experiments mimicking surface and substance, the designers question the stability of our material habitat.",
      link:
        "https://openstudios2021.janvaneyck.nl/participants/jannis-zell-lisa-ertel",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241578/Avatars/RealGardensAI/4GardenTR00031_ivfqwb.png",
      image:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623257844/participant-images/Jannis-Zell-Lisa-Ertel_cbtbt6.jpg",
      layer: "exhibitors",
      positionX: 2682,
      positionY: 1069,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 29,
    },
    {
      exhibitorId: 29,
      focusGroupId: "23_6",
      name: "Lisa Plaut",
      title: "",
      description:
        "“I never thought I would live an historical moment. I feel an amnesia of the present.” Lisa Plaut’s sensuous language responds to her feelings of disembodiment and fragmented time. She draws inspiration from the interior spatial logic of medieval tapestries, wherein a confined unicorn or an anthropophagic woman become figments of a dream held captive.\nIn a new series, Plaut conceives silkscreens on patterned bedsheets as masks. The dialogue between blown-up sketches and the partial camouflage of the bedsheets’ original designs creates friction— at times historical, epidermal, or emotional—between the different layers. Small embellishments, like the bow on Eeyore’s tail or a Frank Lloyd Wright earring, become passkeys to a larger discussion about domestication and self-imposed adaptions to construed social norms.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/lisa-plaut",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241578/Avatars/RealGardensAI/4GardenTR00039_duserh.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2803/51962006_714863608908514_1460783247880086634_n-1-min_2.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 3150,
      positionY: 2880,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 30,
    },
    {
      exhibitorId: 30,
      focusGroupId: "",
      name: "Luisa Puterman",
      title: "",
      description:
        "Little Radio Experiment is an attempt to embrace radio culture as a space to share. Luisa Puterman creates deep listening experiences as a way to foster community and engagement with the environment. During her residency, she gradually developed a 24-hour online radio stream. Its programming includes field recordings and excerpts from Jan van Eyck’s audio archives, as well as current participant contributions and group jam sessions, each one transmitting a different kind of resonance and layered time. For Puterman, radio’s magnetic field is an invisible architecture that facilitates shared moments of collective listening and transmission. Tune in!\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/luisa-puterman",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241579/Avatars/RealGardensAI/4GardenTR00032_qm7pgx.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2804/19_luisa_puterman_1.1500x1500.png",
      layer: "exhibitors",
      positionX: 913,
      positionY: 3128,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 31,
    },
    {
      exhibitorId: 31,
      focusGroupId: "26_6",
      name: "Lukas Rehm",
      title: "Sensory Airplanes / Solastalgia",
      description:
        "The divine twins Castor and Pollux: one a mortal equestrian, the other immortal boxer, whose fates nevertheless become entangled with the stars. Ancient Greek poet Simonides of Ceos once extolled them in a lyric poem intended for Scopas of Thessaly and was severely reprimanded for it. That evening, while called away from Scopas’s banquet hall by a pair of mysterious strangers on horseback, he miraculously eluded the building’s horrific collapse. Guests and host alike were crushed, and it was Simonides who was called upon to identify the deceased from memory. Thereby was born a new calculus for memory. From death and tragedy Simonides founded mnemotechnics, a set of principles to extend the innate cognitive capacity of humankind. In his spatial composition Sensory Airplanes Lukas Rehm summons the twin deities of our time: newly forged neuromorphic computers and the neural networks they mimic. Here the specter of Simonides returns as machine intelligence incarnate, nourished by techno-utopian literature from Silicon Valley. The score, which includes automatized emulations of the baroque opera Castor et Pollux, unfolds at high speeds through a digital audio spatialization system, while an image recognition algorithm tries, succeeds, and fails to analyze the visual flux in real time. Threaded throughout the composition are the words of physicists and computer scientists from the Human Brain Project as well as philosophers Fahim Amir, Dirk Baecker, and Janina Loh. In Sensory Airplanes the origin of mnemotechnics is reimagined in the dark light of machines’ increasing capacity to simulate emotion.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/lukas-rehm",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241578/Avatars/RealGardensAI/4GardenTR00034_pcfe6t.png",
      image:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623246805/participant-images/Lukas-Rehm750x750_whxxud.jpg",
      layer: "exhibitors",
      positionX: 4553,
      positionY: 2112,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 32,
    },
    {
      exhibitorId: 32,
      focusGroupId: "1_7",
      name: "Manjot Kaur",
      title: "",
      description:
        "Manjot Kaur’s drawings, paintings, and time-based media summon the mystical, scientific, and absurd to draw attention to current sociopolitical predicaments and the sovereignty of nature. Borrowed imagery and text bridge the past and present, while imaginative, fantasy-inducing zones of abstraction invite viewers to contemplate the legacies of the landscapes we occupy and exploit. At the root, her works question the very nature of the air that we breathe, the water that we drink, and the food that we eat.\nKaur invokes tales of gods, goddesses, and nayikas (heroines) to comment on social and political realities in India. Crosspollinating ancient mythologies, she counter-narrates iconic motifs and attributes, or sets them in juxtaposition with one another, to allow opportunities for critical reflection. Threaded throughout are revelations of sexuality and fecundity, but also Kaur’s concern about the ever-growing human population and its increasing imbalance with other inhabitants and inheritors of the earth. She asks “What are the implications of fertility and the desire to procreate in present times? How should we respond to current needs while also holding space to think about both human and non-human generations to come?” \nAt times Kaur sets these concerns within metaphorical landscapes that meld images of regeneration, reproduction, cosmology, and microbiology. At the center of these universes are fragile embryos, at turns more diagrammatic or lyrical, interwoven through vegetal and bacterial blooms. As humans and nature continue to adapt to ever-changing, increasingly hostile environments, Kaur’s work offers both dire warnings and insulated, if uncertain, spheres of refuge.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/manjot-kaur",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241578/Avatars/RealGardensAI/4GardenTR00033_rla2qx.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2806/webp_net-resizeimage_5.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 5232,
      positionY: 1361,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 33,
    },
    {
      exhibitorId: 33,
      focusGroupId: "2_7",
      name: "Marielle Chabal",
      title: "",
      description:
        "Marielle Chabal creates fictions of anticipation that orbit worlds within worlds, generating new trajectories of the future tied to current predicaments. She is unambiguous in her conviction that stories participate in the construction of our lives. Her fictions, which she translates into the space-time of an exhibition, a symposium, or a movie, become almost inseparable from the shapes they generate and the actions they engender through her many collaborators. She transforms her ardent desire to change the world into something that approaches embodied experience: the exhibition format becomes an engine that intensifies collective practices by inviting artists, scientists, architects, researchers, and other passionate participants to find new grounds for commonality.\nHer latest project, titled OPP-OPS, expands several years of research and collaboration. Situated in the year 2028, it centers around the fictive OPP-OPS program: a secret, experimental organization—initiated in 1992 and located on a hidden, manmade island—with political aspirations to transform the mentalities of people worldwide. Over time, intimations of its deep roots and extensive reach have been revealed through interviews and archives. According to its fictive builder, Noortje Van Der Laan, OPP-OPS was conceived as a radical means to create a more humane, ecologically just, and equal society. Genetically modified children are raised in seclusion and indoctrinated to be more informed, more capable, and more ethical than the rest of the human population. As adult agents, they are distributed across the globe to infiltrate organs of power and positioned as decision-makers of the world. OPP-OPS explores moral compromise, even evil, as a means to “save the world.”",
      link:
        "https://openstudios2021.janvaneyck.nl/participants/marielle-chabal",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241578/Avatars/RealGardensAI/4GardenTR00037_parmll.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2572/40mcube_mariellechabal_3.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 1297,
      positionY: 3960,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 34,
    },
    {
      exhibitorId: 34,
      focusGroupId: "25_6",
      name: "Maxim Weirich",
      title: "",
      description:
        "As an exhibition designer, curator, and artistic researcher, Maxim Weirich mounts spatial narrations of archival material. His most recent exhibition, The Society of Signs, opened its second iteration at the Museum für Neue Kunst, Freiburg in May. The show traces the modern pictogram from its inception with Otto Neurath (1882–1945) in the 1920s to the present day. It maps how visual symbols have contributed to social and political change, and how their designs have helped articulate some of the greatest cultural transformations of the last century.\nAt the Jan van Eyck, Weirich homes in on one of the exhibition’s key figures, Wolfgang Schmidt (1929-1995). In 1972, Schmidt initiated Lebenszeichen (Signs of Life), an ambitious endeavor to create a pictographic system that could encompass all of human experience. At the time of his death, he had attained 262 of 893 intended pictograms. Through interviews with close friends and collaborators, Weirich has sought to gain insight into the designer’s process and final days. He combines his current research with elements of the exhibition design—including paper flags, free-standing displays, and newly-conceived vitrines—in a personal metacommentary on pictographic languages of the last 100 years and the enigmatic fate of a life cut short.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/maxim-weirich",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241578/Avatars/RealGardensAI/4GardenTR00029_dnptbn.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1624440290/Front-GDZ_acnmkj.jpg",
      layer: "exhibitors",
      positionX: 5100,
      positionY: 2468,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 35,
    },
    {
      exhibitorId: 35,
      focusGroupId: "",
      name: "Mickey Yang",
      title: "",
      description:
        "Mickey Yang mounts an entire oeuvre of ostensible sculpture where nothing is quite what it seems or what meets the eye. She traces her interest in misperceptions and mistranslations from her identity as a Dutch-Singaporean woman and the lack of a common language. “My own cultural heritage is a form of unconscious impersonation,” she writes. The carefully choreographed kinetic sculptures and video of her recent series Upaya (2020) playfully adopt Asian philosophies and spiritual practices distorted and diluted for Western consumption. Four metal panels in wooden frames, made in close collaboration with craftsmen, depict imagery drawn from the origins of Chinese characters, originally conceived as pictograms. Two other large forged metal gatekeepers on wheels slide across the floor at different intervals prohibiting easy trespass. Nevertheless, Yang welcomes visitors with warmth and humor to question cultural assumptions. Her latest work continues to build on Upaya, bringing in archeological features as well as more sensorial elements, to construct entire atmospheres. Through veils of light and smoke, viewers find new ways to navigate once familiar terrain. \n",
      link: "https://openstudios2021.janvaneyck.nl/participants/mickey-yang",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241578/Avatars/RealGardensAI/4GardenTR00030_eqegpo.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2810/mkyang-1.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 2597,
      positionY: 5296,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 36,
    },
    {
      exhibitorId: 36,
      focusGroupId: "1_7",
      name: "Niina Tervo",
      title: "",
      description:
        "Niina Tervo’s intuitive sculptures explore our relationship to and communication with the Other—both animate and inanimate—through our bodies and what she calls the intangible world. Electrical energy circulates within us, creating thoughts and emitting heat. It is composed of waves that traverse lightyears and cross the boundaries of our skin, possibly retaining impressions of each past encounter. As these accumulated impressions coalesce within and are released by us, they raise questions about the boundaries of selfhood.\nTervo’s multisensory sculptures open our bodies and minds to hidden ways of responding to this Other. Often, she uses materials that disintegrate or dissipate over time. Through the process of molding and seeing what emerges, she draws out her own emotions and memories, while absorbing the energy of the raw material. The result is a gesture molded of self and matter.\nAmong her latest works, on view as a part of an installation, is a sculpture made of eggplant skin, accompanied by a text by writer and poet Milka Luhtaniemi. Tervo used the vegetable’s hollowed-out flesh to make baba ghanoush, which she served to Luhtaniemi. In eating and metabolizing it, the author transformed the flesh into written word. Together, the artists explored the information Tervo could impart in the making of her purée and that which Luhtaniemi could glean from eating it. For Tervo, the work is about inviting someone to think along with and lead thought in a new direction, thereby opening a pathway to new storylines.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/niina-tervo",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241578/Avatars/RealGardensAI/4GardenTR00036_dbaa37.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1623680116/niina_hkjjgf.jpg",
      layer: "exhibitors",
      positionX: 2737,
      positionY: 1594,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 37,
    },
    {
      exhibitorId: 37,
      focusGroupId: "2_7",
      name: "Nina Nowak",
      title: "",
      description:
        "Nina Nowak's work questions the boundaries between objects and bodies, and between inanimate and animate beings. She brings these observations to bear on the vast tunnel networks carved throughout mines. For Nowak, a mine is an object that is stretched in time and space to such great magnitude that it becomes impossible to grasp at once in its entirety. \nAt Open Studios, Nowak’s multilayered installation attempts to bridge the chasm between dimension and scale. Large silkscreens depicting a fictional story serve as a red thread across the different media. They tell a tale of geology, bodies, and processes of transformation. They also tell a tale of dimensions in space and time beyond human apprehension. The narrative comes together in a group of works that combines prints with mechanical tools, 3D sculpture, animation, installation, and stone carving.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/nina-nowak",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241579/Avatars/RealGardensAI/4GardenTR00028_av8lwy.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2811/ninanowak_circadianrhythm_web1.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 841,
      positionY: 257,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 38,
    },
    {
      exhibitorId: 38,
      focusGroupId: "25_6",
      name: "Offshore Studio",
      title: "",
      description: "Are you scared of wolves?",
      link:
        "https://openstudios2021.janvaneyck.nl/participants/offshore-studio",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241578/Avatars/RealGardensAI/4GardenTR00035_nwltn5.png",
      image:
        "https://res.cloudinary.com/a-m/image/upload/v1623680208/Offshore_Managing_the_wild_02_znrnqr.jpg",
      layer: "exhibitors",
      positionX: 2703,
      positionY: 4043,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 39,
    },
    {
      exhibitorId: 39,
      focusGroupId: "3_7",
      name: "Pejvak",
      title: "",
      description:
        "Pejvak is the long-term collaboration between Felix Kalmenson and Rouzbeh Akhbari. Their latest endeavor, If Need Be, narrativizes water infrastructures and the mythological dimensions of geopolitical conflicts tied to resource access. While planetary in its scope, their constellational research pays special attention to grand narratives and micro-histories of water management in central Asia. Since the disintegration of the Soviet Union, conflicting needs and priorities between upstream and downstream polities have caused long-lasting water and energy disputes. Tracing the contemporary resource struggles in the region to a broad set of geopolitical interconnections—namely the Iranian constitutional revolution; the so-called Great Game and its labyrinth of counterintelligence activities; and the US’s anti-desertification agendas under President Franklin Roosevelt—Kalmenson and Akhbari look at the complex psychological, literary, and performative dimensions they engender. \nAs part of this project, Pejvak has created an experimental literary work. An edited ficto-critical volume, it brings together a body of speculative narratives that pursue six historical and contemporary characters whose lives directly or indirectly intersect with modes of resource access and control in the region. Combining their own writing with commissioned stories, the trilingual publication (Russian, Farsi, and English) is designed by fellow Jan van Eyck participant Rudy Guedj. With an emphasis on fiction, microhistories, and the ways in which they mobilize grander national narratives, If Need Be acts much like a holy landscape whereby the reader, not unlike a flowing river, falls through the storyline’s cracks only to reemerge at the surface by intention or otherwise.",
      link: "https://openstudios2021.janvaneyck.nl/participants/pejvak",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241577/Avatars/RealGardensAI/4GardenTR00038_swbhpu.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2812/webp_net-resizeimage_2.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 3990,
      positionY: 4248,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 40,
    },
    {
      exhibitorId: 40,
      focusGroupId: "26_6",
      name: "Rudy Guedj",
      title: "",
      description:
        "Close up of the door handle. \nThe camera moves steadily downward, finally resting on a keyhole.\nIn an instant, the room’s darkness is pulverized by the excess of light.\n\nIn film, a knob or door handle arouses childhood fears of a bedroom door shut at night. As a cinematic trope, it is an obvious but efficient ploy to create tension and maintain suspense. It taps into the terror of that which cannot be seen but only dreaded. Most of all, it works as a signifier for possible shifts in narrative arch. It symbolizes a passage in the storyline. Recently, the door handle has also come to emblematize the stigma it once bore in silence. It has become frightening and untouchable, its original purpose seemingly defeated. \nIn a building, doors and windows are thresholds between inside and outside, between light and darkness, between observer and observed. For this presentation Rudy Guedj creates a space in constant transition by placing these two architectural apertures in dialogue. By turning windows into projecting devices and doors into screens, he explores the potential of analogy and synchronicity in the creation of narratives.\n",
      link: "https://openstudios2021.janvaneyck.nl/participants/rudy-guedj",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241577/Avatars/RealGardensAI/4GardenTR00043_cjq8gj.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2813/rg_windows-1.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 5053,
      positionY: 1972,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 41,
    },
    {
      exhibitorId: 41,
      focusGroupId: "25_6",
      name: "Savas Boyraz",
      title: "",
      description:
        "Through photography and film, Savaş Boyraz seeks ways of creating alliances with nature in resistance. Turkey has long suffocated Kurdish territories of their natural resources. Dams are erected to block water passage, forests set ablaze to increase visibility, and wetlands drained to make land more accessible to military aircraft. Animal populations are decimated, and semi-nomadic farming processes rendered untenable.\nAs native animal species are pushed out or destroyed, new, so-called invasive species move in. They take the shape of F-16 Fighting Falcons, IAI Herons, Leopards I & II, or the Otokar Cobra. These military weapons appropriate the names and features of animals who and upon whom they wreak havoc. Boyraz, for his part, looks for ways in which native animal and human populations fight back. Partridge Nation conjures the tradition of using caged partridge birds to lure and capture those in the wild. Its title alludes to Turkey’s history of manipulating internal conflict among Kurds to suppress uprisings. Boyraz adopts these partridges as a symbol of cultural identity and traces local vocal practices that imitate their and other bird’s croons. Under a regime that designates the Kurdish language as “unintelligible,” singing in Kurdish becomes akin to bird singing. Both become instances of cultural reclamation and defiance.",
      link: "https://openstudios2021.janvaneyck.nl/participants/savas-boyraz",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241577/Avatars/RealGardensAI/4GardenTR00038_swbhpu.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2814/the_state_we_are_in_02s.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 1532,
      positionY: 2463,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 42,
    },
    {
      exhibitorId: "",
      focusGroupId: "",
      name: "",
      title: "",
      description: "",
      link: "",
      avatar: "",
      image: "",
      layer: "",
      positionX: "",
      positionY: "",
      sizeW: "",
      sizeH: "",
      isPublished: true,
      id: 43,
    },
    {
      exhibitorId: 43,
      focusGroupId: "26_6",
      name: "Sophia Holst",
      title: "",
      description:
        "“…in this complex, housing is intended for an already evolved population, prepared for the use of a correct living space.” Published in the architecture magazine La Maison in 1955, these words were used to describe the newly erected social housing blocks between Rue Haute and Rue de la Philanthropie in central Brussels. Lauded as modernist masterpieces, they promised to provide so-deemed cleaner, more evolved members of the lower classes a home in the city center. Almost seventy years later, these buildings still stand. Now greatly deteriorated and stigmatized, they are a last refuge for marginal and distressed communities. In her project Cité de la Philanthropie/Stad van Menslievendheid, architect and researcher Sophia Holst exposes dire living conditions in Brussels’ public housing as an impetus to redefine social ideals in architecture and urban (re)development. \n“A failure of democracy,” states one resident plainly. “Miserable hiding places,” says another. Even now that the Brussels Region has finally taken steps to renovate these dwellings, the modernist project of social conditioning remains intact. Under a new guise of social cohesion, apartments are being resized and reconfigured to accommodate different and fewer households. Many residents who have been relocated to the Brussels’ periphery during construction will be unable to return. In the name of climate change, facades are also being revamped. But behind golden balconies and silver cladding, the standard of living remains unchanged. \nBrussels’ public housing administrations too remain shrouded in opacity. From the onset of her project, Logement Bruxellois has refused to share data with Holst. In expressing the lack of empathy and interest in human welfare at the center of social housing, a tenant has the final word: “Even dogs are treated better.” \n",
      link: "https://openstudios2021.janvaneyck.nl/participants/sophia-holst",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241576/Avatars/RealGardensAI/4GardenTR00048_ktqoyy.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2815/webp_net-resizeimage.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 2092,
      positionY: 211,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 44,
    },
    {
      exhibitorId: 44,
      focusGroupId: "25_6",
      name: "The Soft Protest Digest",
      title: "",
      description:
        "The art and research collective Soft Protest Digest is composed of artist Nickie Sigurdsson and designers and researchers Robin Bantigny and Jérémie Rentien Lando. Together they work toward unearthing regenerative food webs, taking as their starting point the histories and contexts of the landscapes in which they find themselves. \nLargely confined to the Jan van Eyck during their residency, the collective adopted an introspective approach orbiting questions of transition and self-sustenance in dire circumstances, such as pandemics and planetary crises. Sigurdsson planted a garden on the academy’s grounds that could supply the Jan van Eyck Café-Restaurant using heirloom produce not currently represented in the EU’s common catalogue of varieties of vegetable species. Through the process of cultivation, she observed how gardens are both a living conservatory of stories connecting humans to spiritual technologies and vegetal allies as well as host to structural inequalities and exploitative legacies. \nIn return, Robin Bantigny has made and ripened cheeses from locally produced raw milk to serve daily for one month at the Café-Restaurant. He took pains to record his extensive cycling journeys around the region to dairy farmers, from whom he also collected soil samples to study their biota. These deliberative performative routines, like those of Sigurdsson’s communal gardening, entangled him in the landscape’s rhythms of mutualism, growth, and decay.\n",
      link:
        "https://openstudios2021.janvaneyck.nl/participants/the-soft-protest-digest",
      avatar:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623241577/Avatars/RealGardensAI/4GardenTR00041_yukaem.png",
      image:
        "https://janvaneyck.nl/site/assets/files/2800/spd1019_team-building-b.1500x1500.jpg",
      layer: "exhibitors",
      positionX: 5024,
      positionY: 5302,
      sizeW: 300,
      sizeH: 300,
      isPublished: true,
      id: 45,
    },
    {
      exhibitorId: 100,
      focusGroupId: "",
      name: "Camp Fire",
      title: "General Chat",
      description: "",
      link: "",
      avatar: "https://media.giphy.com/media/TdpjDZfxgFyDhY4rwY/giphy.gif",
      image: "",
      layer: "exhibitors",
      positionX: 2700,
      positionY: 2700,
      sizeW: 600,
      sizeH: 600,
      isPublished: true,
      id: 46,
    },
    {
      exhibitorId: 46,
      focusGroupId: "",
      name: "Spring Brunnen",
      title: "",
      description: "",
      link: "",
      avatar: "",
      image: "http://cdn.viennastruggle.at/janvaneyk/tile-chains.png",
      layer: "default",
      positionX: 0,
      positionY: 600,
      sizeW: 600,
      sizeH: 600,
      isPublished: true,
      id: 47,
    },
    {
      exhibitorId: 47,
      focusGroupId: "",
      name: "[BackgroundLayer]",
      title: "[BackgroundLayer]",
      description: "",
      link: "",
      avatar: "",
      image:
        "https://res.cloudinary.com/ddddty57j/image/upload/v1623262449/bg/fourthgarden-bg-10_oyqfzx.jpg",
      layer: "background",
      positionX: "",
      positionY: 600,
      sizeW: "",
      sizeH: "",
      isPublished: true,
      id: 48,
    },
  ],
}
