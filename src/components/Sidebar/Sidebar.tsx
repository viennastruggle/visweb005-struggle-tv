import styled from "styled-components";

const Sidebar = styled.aside`
  border: none;
  position: fixed;
  top: 4rem;
  left: 0;
  width: 15rem;
  max-height: calc(100vh - 9rem);
  overflow: auto;
  background: var(--card-background);
  box-shadow: var(--shadow-blur);
  margin: 1rem;

  border-radius: 1rem;
  & > div:not(:first-child) {
    border-top: 1px solid rgba(0,0,0,0.3);
  }
`
export default Sidebar;