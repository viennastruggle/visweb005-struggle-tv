import styled from "styled-components";

export const NameTag = styled.div`
  margin-top: .25rem;
  border-radius: 1rem;
  font-weight: 500;
  color: var(--text);
  background: var(--card-background);
  border: 1px solid var(--card-background);
  text-align: center;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 2rem;
  padding: 0;
  box-shadow: var(--shadow-blur);
  &:hover {
    cursor: text;
    background: var(--card-background);
    border: 1px solid black;
  }
`;
