import Picker, { SKIN_TONE_MEDIUM_DARK } from "emoji-picker-react";
import React from "react";
import styled from "styled-components";

const Container = styled.div`
  .emoji-picker-react {
    border-radius: 0 !important;
    border: none;
    width: auto !important;
  }
`
const EmojiData = ({ chosenEmoji }) => (
  <div>
    {chosenEmoji.emoji}
    {/* <strong>Unified:</strong> {chosenEmoji.unified}
    <br />
    <strong>Names:</strong> {chosenEmoji.names.join(", ")}
    <br />
    <strong>Symbol:</strong> {chosenEmoji.emoji}
    <br />
    <strong>ActiveSkinTone:</strong> {chosenEmoji.activeSkinTone} */}
  </div>
)


const EmojiPicker = ({chosenEmoji,onEmojiClick}) => {
    return (
    <Container>
        {chosenEmoji && <EmojiData chosenEmoji={chosenEmoji} />}
        <Picker
          onEmojiClick={onEmojiClick}
          disableAutoFocus={true}
          skinTone={SKIN_TONE_MEDIUM_DARK}
          groupNames={{ smileys_people: "PEOPLE" }}
          native
        />
      </Container>
    )
}

export default EmojiPicker;