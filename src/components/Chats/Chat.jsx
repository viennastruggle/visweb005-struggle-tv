import { useAuth0 } from "@auth0/auth0-react"
import firebase from "firebase/app"
import React, { useEffect, useRef, useState } from "react"
import { useCollectionData } from "react-firebase-hooks/firestore"
import { FiArrowUp, FiExternalLink } from "react-icons/fi"
import { IoMdClose } from "react-icons/io"
import styled from "styled-components"
import { useConferenceStore } from "../../store/ConferenceStore"
import { useLocalStore } from "../../store/LocalStore"
import ExhibitorDetail from "../Flags/Presets/ExhibitorDetail"
import { useExhibitorStore } from "../Flags/Presets/ExhibitorStore"
import ChatMessage from "./ChatMessage"
import EmojiPicker from "./EmojiPicker"
import { firestore } from "./ExhibitorStoreLogic"

export const messagePrefix = "thefourthgarden_";

const Container = styled.div`
  --chat-width: 40rem;
  position: fixed;
  bottom: 4rem;
  height: calc(100vh - 8rem);
  width: var(--chat-width);
  left: calc(50vw - calc(var(--chat-width) / 2));
  background-color: var(--card-background);
  background-image: var(--card-background-image);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  opacity: 0.9;
  z-index: 100000;
  display: flex;
  flex-direction: column;
  border-radius: 1rem;
  transition: all 0.5s;
  &:hover {
    background-color: var(--card-background);
    box-shadow: var(--shadow-blur);
    opacity: 1;
  }
  .title {
    font-size: 1.5rem;
    padding: 1rem 2.5rem;
  }
  .messages {
    flex: 100%;
    overflow: auto;
    padding: 0 2.5rem;
    text-align: left;
    img {
      max-width: 100%;
    }
  }
  .form {
    display: flex;
    align-items: center;
    margin: 1rem 2.5rem;
    background: rgba(196, 196, 196, 0.4);
    border-radius: 2rem;
    input {
      flex: 100%;
      height: 2rem;
      line-height: 2rem;
      background: transparent;
      border: none;
      padding: 0 1rem;
      font-size: 1rem;
      box-sizing: border-box;
      outline: none;
      &:focus {
        background: rgba(255,255,255,0.1);
      }
    }
    button {
      font-size: 1.5rem;
      border: 0;
      display: flex;
      align-items: center;
      justify-content: center;
      height: 2rem;
      background: transparent;
      text-transform: uppercase;
      cursor: pointer;
      svg {
        background: #3e97ff;
        border-radius: 1rem;
        color: white;
      }
      &:disabled {
        opacity: 0.8;
      }
      &:hover {
        color: black;
      }
      &[type="button"],
      &.keyboardButton {
        border-radius: 1rem;
      }
    }
  }
`

function Chat() {
  const activeExhibitor = useExhibitorStore((store) => store.activeExhibitor)
  return <>{activeExhibitor && <ChatRoom />}</>
}

const StyledClose = styled(IoMdClose)`
  position: absolute;
  right: 2rem;
  top: 1rem;
  cursor: pointer;
  font-size: 2rem;
`
function ChatRoom() {
  const { user, isAuthenticated } = useAuth0()
  const messagesRef = firestore.collection("messages")
  const query = messagesRef.orderBy("createdAt", "desc").limitToLast(1000)

  const [messages] = useCollectionData(query, { idField: "id" })
  const [formValue, setFormValue] = useState("")

  const [additionalKeyboard, setAdditionalKeyboard] = useState("")

  /* Get Display Name from Store*/
  const displayName = useConferenceStore((store) => store.displayName)
  const setDisplayName = useConferenceStore((store) => store.setDisplayName)
  useEffect(() => {
    if (isAuthenticated) setDisplayName(user?.name ?? "")
  }, [user, isAuthenticated, setDisplayName])

  /* Set active Exihibitor */
  const activeExhibitor = useExhibitorStore((store) => store.activeExhibitor)
  const setActiveExhibitor = useExhibitorStore(
    (store) => store.setActiveExhibitor,
  )
  const id = useLocalStore((store) => store.id)

  /* Emoji keyboard */
  const [chosenEmoji, setChosenEmoji] = useState(null)
  const inputEl = useRef(null)
  const onEmojiClick = (event, emojiObject) => {
    setFormValue(formValue + emojiObject.emoji)
    setChosenEmoji(emojiObject)
    inputEl.current.focus()
  }

  /* Scrolling to first message */
  const dummy = useRef()

  const scrollToChat= () => {
    dummy.current.scrollIntoView({ behavior: "smooth" })
  }

  const handleKeyboardChange = () => {
    if (additionalKeyboard === "emoji") setAdditionalKeyboard("")
    else setAdditionalKeyboard("emoji")
  }

  const sendMessage = async (e) => {
    e.preventDefault()

    await messagesRef.add({
      user: displayName,
      exhibitorId: messagePrefix + activeExhibitor.exhibitorId,
      body: formValue,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      uid: id,
      photoURL: "",
    })
    scrollToChat();
    setFormValue("");

  }

  return (
    <Container>
      <div className="title">
        <span>{activeExhibitor?.name}</span>{" "}
        {activeExhibitor?.link && (
          <a href={activeExhibitor?.link} rel="noreferrer" target="_blank">
            <FiExternalLink /> see more
          </a>
        )}
        <span onClick={() => setActiveExhibitor(undefined)}>
          <StyledClose />
        </span>
      </div>
      <div className="messages">
        <ExhibitorDetail exhibitor={activeExhibitor} />
        <span ref={dummy}></span>
        {messages &&
          messages.map((msg) => {
            if (msg.exhibitorId === messagePrefix + activeExhibitor.exhibitorId)
              return (
                <ChatMessage
                  key={msg.id}
                  message={msg}
                  isLocal={msg.user === displayName}
                />
              )
            return null
          })}
      </div>

      {additionalKeyboard && (
        <EmojiPicker onEmojiClick={onEmojiClick} chosenEmoji={chosenEmoji} />
      )}
      <form onSubmit={sendMessage} className="form">
        <button
          className="keyboardButton"
          type="button"
          onClick={handleKeyboardChange}
        >
          {additionalKeyboard === "emoji" ? "😶" : "😃"}
        </button>
        <input
          value={formValue}
          ref={inputEl}
          onChange={(e) => setFormValue(e.target.value)}
          placeholder={`Leave a message ...`}
        />
        <button type="submit" disabled={!formValue}>
          <FiArrowUp />
        </button>
      </form>
    </Container>
  )
}

export default Chat
