import dayjs from "dayjs"
import firebase from "firebase/app"
import "firebase/firestore"
import { FunctionComponent, useEffect, useState } from "react"
import { data } from "../../mocks/fourthgarden/exhibitors"
import { focusGroups } from "../../mocks/fourthgarden/foucsGroups"
import { useExhibitorStore } from "../Flags/Presets/ExhibitorStore"
import { useLoadingStore } from "../Flags/Presets/LoadingStore"

const firebaseConfig = {
  apiKey: "AIzaSyDS-Edha_U7Z46PKJ5RFB2aXvl6MED1J6A",
  authDomain: "vistoo002-chats.firebaseapp.com",
  projectId: "vistoo002-chats",
  storageBucket: "vistoo002-chats.appspot.com",
  messagingSenderId: "790762754038",
  appId: "1:790762754038:web:47cef328fd4240890c81d7",
}

firebase.initializeApp(firebaseConfig)
export const firestore = firebase.firestore()

export const getRandomNumbers = (min: number, max: number) => {
  return [
    Math.floor(Math.random() * (max - min + 1)) + min,
    Math.floor(Math.random() * (max - min + 1)) + min,
  ]
}

const filterLayer = (data, layer: string) =>
  data.filter((ex) => ex.isPublished && ex.layer === layer)

export const ExhibitorStoreLogic: FunctionComponent = () => {
  const setLayers = useExhibitorStore((store) => store.setLayers)
  const setFocusGroup = useExhibitorStore((store) => store.setFocusGroups)
  const setActiveFocusGroup = useExhibitorStore(
    (store) => store.setActiveFocusGroup,
  )
  const setIsLoading = useLoadingStore((store) => store.setIsLoading)

  const [isMounted, setIsMounted] = useState(true)

  useEffect(() => {
    // TODO Refactor to use-query
    const getExhibitors = async () =>
      setLayers([
        {
          id: "exhibitors",
          items: filterLayer(data.exhibitors, "exhibitors"),
        },
        { id: "default", items: filterLayer(data.exhibitors, "default") },
        {
          id: "background",
          items: filterLayer(data.exhibitors, "background"),
        },
      ])
    setIsMounted(false)
    setIsLoading(false)
    if (isMounted) {
      setIsLoading(true)
      getExhibitors()
    }
  }, [setLayers, setIsMounted, isMounted, setIsLoading])

  useEffect(() => {
    // TODO Refactor to use-query
    const getFocusGroups = async () => setFocusGroup(focusGroups)
    const found = focusGroups.find(
      (focusGroup) =>
        dayjs(focusGroup.startDate) <= dayjs() &&
        dayjs(focusGroup.endDate) >= dayjs(),
    )
    if (found) setActiveFocusGroup(found)
    setIsMounted(false)
    setIsLoading(false)

    if (isMounted) {
      setIsLoading(true)
      getFocusGroups()
    }
  }, [
    setFocusGroup,
    setActiveFocusGroup,
    setIsMounted,
    isMounted,
    setIsLoading,
  ])

  return null
}
