import React from "react"
import styled from "styled-components"
import { useConferenceStore } from "../../store/ConferenceStore"
import ChatMessage from "./ChatMessage"

const Container = styled.div`
  height: 90%;
  overflow: auto;
  padding: 0 1rem;
`

export const ChatMessagePanel = () => {
  const conferenceStore = useConferenceStore()

  const userMap = new Map<string, string>()
  Object.entries(conferenceStore.users).forEach((user) => {
    userMap.set(user[0], user[1].user._displayName)
  })
  const fieldRef = React.useRef<HTMLInputElement>(null)
  React.useEffect(() => {
    if (fieldRef.current) {
      fieldRef.current.scrollIntoView({ behavior: "smooth" })
    }
  }, [conferenceStore.messages])
  return (
    <Container>
      {conferenceStore.messages.map((message, i, list) => {
        if (i === list.length - 1) {
          return (
            <div ref={fieldRef}>
              <ChatMessage
                key={message.time + message.user}
                isLocal={false}
                message={{
                  time: message.time,
                  body: message.message,
                  user: userMap.has(message.user)
                    ? userMap.get(message.user)
                    : message.user,
                }}
              />
            </div>
          )
        } else {
          return (
            <ChatMessage
              isLocal={false}
              key={message.time + message.user}
              message={{
                time: message.time,
                body: message.message,
                user: userMap.has(message.user)
                  ? userMap.get(message.user)
                  : message.user,
              }}
            />
          )
        }
      })}
    </Container>
  )
}

// {isValidHttpUrl(messageObj.message)
//     ? LinkMessage(messageObj.message)
//     : messageObj.message}

// function isValidHttpUrl(string) {
//   let url

//   try {
//     url = new URL(string)
//   } catch (_) {
//     return false
//   }

//   return url.protocol === "http:" || url.protocol === "https:"
// }

// const LinkMessage = (message: string) => {
//   return (
//     <a target="_blank" href={message}>
//       {message}
//     </a>
//   )
// }
