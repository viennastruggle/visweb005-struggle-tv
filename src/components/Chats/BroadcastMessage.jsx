import { useEffect } from "react"
import { useToasts } from "react-toast-notifications"
import useSound from "use-sound"
import { useConferenceStore } from "../../store/ConferenceStore"
import fanfare from "./../../assets/sounds/fanfare.mp3"

function BroadcastMessageLogic() {
  const { addToast } = useToasts()
  const [play] = useSound(fanfare, { volume: 0.3 })
  const messages = useConferenceStore((store) => store.messages)

  useEffect(() => {
    if (messages.length > 0) {
      play()
      addToast(messages[messages.length - 1].message, {
        appearance: "info",
        autoDismiss: true,
      })
    }
  }, [messages, addToast, play])

  return <></>
}

export default BroadcastMessageLogic
