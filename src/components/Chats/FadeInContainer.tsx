import { motion } from "framer-motion";

const variants = {
  enter: {
    opacity: 0,
    scale: 0,
  },
  center: {
    opacity: 1,
    scale: 1,
  },
  exit: {
    opacity: 0,
    scale: 0,
  },
};

const FadeInContainer = ({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}) => (
    
  <motion.div
    variants={variants}
    initial="enter"
    animate="center"
    exit="exit"
    transition={{
      opacity: { duration: 0.3 },
    }}
  >
    {children}
  </motion.div>
);

export default FadeInContainer;
