import { useAuth0 } from "@auth0/auth0-react"
import React from "react"
import { FiArrowDownCircle, FiArrowUp, FiArrowUpCircle } from "react-icons/fi"
import styled from "styled-components"
import { useConferenceStore } from "../../store/ConferenceStore"
import { useLocalStore } from "../../store/LocalStore"
import { ChatMessagePanel } from "./ChatMessagePanel"

const Container = styled.div`
  display: flex;
  flex-direction: column;
  transition: all 0.5s;
  header {
    button {
      width: 100%;
      display: flex;
      flex-direction: row;
      align-items: stretch;
      div:first-of-type {
        flex: auto;
        text-align: left;
        padding: 0.5rem 1rem;
      }
      div:last-of-type {
        padding: 0.5rem 1rem;
        align-items: center;
        text-align: right;
        display: flex;
        justify-content: flex-end;
      }
    }
  }
  button {
    line-height: 2rem;
    pointer-events: auto;
    transition: all 0.2s;
    border: none;
    background: none;
    cursor: pointer;

    &.active {
      color: #0000ff;
      font-size: 1rem;

      border: none;
    }
    font-size: 1rem;
    margin: 0;
  }
  
  .title {
    font-size: 1.5rem;
    padding: 1rem 2.5rem;
  }
  .messages {
    flex: 100%;
    overflow: auto;
    padding: 0 2.5rem;
    text-align: left;
    img {
      max-width: 100%;
    }
  }
  .form {
    display: flex;
    align-items: center;
    margin: 1rem 1rem;
    background: rgba(196, 196, 196, 0.4);
    border-radius: 2rem;
    input {
      flex: 100%;
      height: 2rem;
      line-height: 2rem;
      background: transparent;
      border: none;
      padding: 0 1rem;
      font-size: 1rem;
      box-sizing: border-box;
      outline: none;
      &:focus {
        background: rgba(255, 255, 255, 0.1);
      }
    }
    button {
      font-size: 1.5rem;
      border: 0;
      display: flex;
      align-items: center;
      justify-content: center;
      height: 2rem;
      background: transparent;
      text-transform: uppercase;
      cursor: pointer;
      svg {
        background: #3e97ff;
        border-radius: 1rem;
        color: white;
      }
      &:disabled {
        opacity: 0.8;
      }
      &:hover {
        color: black;
      }
      &[type="button"],
      &.keyboardButton {
        border-radius: 1rem;
      }
    }
  }
`

export const ChatPanel = () => {
  const conferenceStore = useConferenceStore()
  const { user, isAuthenticated, isLoading } = useAuth0()
  const [isActive, setIsActive] = React.useState<boolean>(false)
  const [unreadMessages, clearMessage] = React.useState<number>(0)
  const localStore = useLocalStore()
  const onInputChange = (e) => {
    localStore.setLocalText(e.target.value)
  }

  const handleSubmit = (e) => {
      e.preventDefault();
    conferenceStore.sendTextMessage(localStore.text)
    localStore.setLocalText("")
  }

  const onKeyDown = (e) => {
    if (e.keyCode === 13) {
      handleSubmit(e)
    }
  }

  const toggleCollapse = (e) => {
    setIsActive(!isActive)
    conferenceStore.clearUnreadMessages()
  }
  if(!isAuthenticated) return <></>
  return (
    <Container>
      <header>
        <button onClick={toggleCollapse}>
          <div>Broadcast Chat {isActive ? "" : <ChatCount></ChatCount>}</div>
          <div>{isActive ? <FiArrowUpCircle /> : <FiArrowDownCircle />}</div>
        </button>
      </header>

      {isActive && (
        <>
          <ChatMessagePanel />
          <form className="form" onSubmit={handleSubmit}>
            <input
              type="text"
              placeholder="Type here ..."
              onChange={onInputChange}
              onKeyDown={onKeyDown}
              value={localStore.text}
            />
            <button type="submit" disabled={localStore.text === ""}>
          <FiArrowUp />
        </button>
          </form>
        </>
      )}
    </Container>
  )
}

const ChatCount = () => {
  const conferenceStore = useConferenceStore()
  return <>({conferenceStore.unreadMessages.toString()})</>
}
