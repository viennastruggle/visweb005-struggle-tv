import clsx from "clsx"
import React from "react"
import ReactMarkdown from "react-markdown"
import styled from "styled-components"

const Container = styled.div`
  > div {
    text-align: left;
    position: relative;
    font-size: 1rem;
    line-height: 1.25rem;
    display: flex;
    margin-bottom: 1rem;
    align-items: flex-start;

    .user,
    .time {
      color: var(--text);
      font-size: 0.75rem;
      line-height: 1.25rem;
      transition: all 2s;
      border-radius: 1rem;
      opacity: 1;
      padding: 0 0.5rem;
      margin-right: 1rem;
      white-space: nowrap;
      z-index: 1;
      pointer-events: none;
      background: var(--chat-incoming-message-background);
    }
    .time {
      background: white;
    }
    .isLocal .user {
      background: var(--chat-outgoing-message-background);
    }
    .body {
      color: black;
      font-size: 1rem;
      flex: 100%;
      line-height: 1.25rem;
      h1,
      h2,
      h3,
      h4,
      h5,
      h6,
      p,
      ul,
      li {
        display: inline;
        font-size: 1rem;
        line-height: 1.25rem;
      }
      hr {
        border: 0.5rem solid black;
        background: black;
      }
    }
  }
`

const ChatMessage = ({ message, isLocal }) => {
  return (
    <Container>
      <div className={clsx({ isLocal })}>
        <div className="user">
          {message.time && (
            <span className="time">
              {message.time.getHours().toString().padStart(2, "0")}:
              {message.time.getMinutes().toString().padStart(2, "0")}:
              {message.time
                .getSeconds()
                .toString()
                .padStart(2, "0")
                .concat("  ")}
            </span>
          )}
          {message.user}
        </div>
        <ReactMarkdown className="body">{message.body}</ReactMarkdown>
      </div>
    </Container>
  )
}

export default ChatMessage
