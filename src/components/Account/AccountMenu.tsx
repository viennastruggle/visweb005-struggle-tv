import { useAuth0 } from "@auth0/auth0-react"
import { useState } from "react"
import { FiInfo } from "react-icons/fi"
import styled from "styled-components"
import LoginButton from "./LoginButton"
import LogoutButton from "./LogoutButton"

const Container = styled.div`
  
  padding: 1rem;
  font-size: 1rem;
  line-height: 1rem;
  header {
    display: flex;
    gap: 0.5rem;
    > div:last-of-type {
      display: flex;
      align-items: center;
      justify-content: center;
    }
  }
  section {
    margin-top: 1rem;
  }
  hr {
    border: none;
    height: 1px;
    background: black;
  }
  p {
    margin-top: 0;
  }
  img {
    border-radius: 1rem;
    width: 2rem;
    height: 2rem;
    object-fit: cover;
  }
  menu {
    margin: 0;
    padding: 0;
    button {
      margin-bottom: 0.5rem;
      &:last-child {
        margin: 0;
      }
    }
  }
  button {
    border-radius: 1rem;
    border: 0;
    height: 1.5rem;
    width: 100%;
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    &:hover {
      color: var(--second);
      background: var(--text);
    }
  }
`

const AccountMenu = () => {
  const { user, isAuthenticated, isLoading } = useAuth0()
  const [isCollapsed, setIsCollapsed] = useState(true)

  if (isLoading) return <></>
  if (!isAuthenticated)
    return (
      <Container>
        <header onClick={() => setIsCollapsed(!isCollapsed)}>
          <div>
            <FiInfo />
          </div>
          <div>Instructions</div>
        </header>
        {!isCollapsed && (
          <section>
            <p>
              Explore the artists growing in the Fourth Garden by clicking on
              their names.
            </p>
            <p>
              Drag your bubble around and zoom in and out to explore the map.
            </p>
            <p>
              Add your name underneath your bubble so we can meet and chat
              together.
            </p>
            <p>Check our program, the unexpected can happen...</p>
            <hr />
            <p>Save your name?</p>
            <LoginButton />
          </section>
        )}
      </Container>
    )
  return (
    <Container>
      <header onClick={() => setIsCollapsed(!isCollapsed)}>
        <div>
          <img src={user?.picture} alt={user?.name} />
        </div>
        <div>
          <div>
            {user?.name}
          </div>
        </div>
      </header>
      {!isCollapsed && (
        <section>
          <menu>
            <LogoutButton />
          </menu>
        </section>
      )}
    </Container>
  )
}

export default AccountMenu
