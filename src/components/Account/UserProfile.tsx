import { useAuth0 } from "@auth0/auth0-react"
import { FC } from "react"
import styled from "styled-components"
import Navigation from "../Header/Navigation"

const Container = styled.section`
  background: var(--background);
  padding: 3rem;
`

const Profile: FC = () => {
  const { user, isAuthenticated, isLoading } = useAuth0()

  if (isLoading) {
    return <div>Loading ...</div>
  }

  if (!user) return <></>
  return (
    <>
      <Navigation />
      <Container>
        {isAuthenticated ? (
            <div>
            <img src={user.picture} alt={user.name} />
            <h2>{user.name}</h2>
            <p>{user.email}</p>
            </div>
        ) : (
            "No Permission"
        )}
      </Container>
    </>
  )
}

export default Profile
