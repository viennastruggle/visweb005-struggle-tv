import { useAuth0 } from "@auth0/auth0-react"
import { FiEdit } from "react-icons/fi"
import { useHistory } from "react-router-dom"
import styled from "styled-components"
import { useConferenceStore } from "../../store/ConferenceStore"
import { useConnectionStore } from "../../store/ConnectionStore"

const Container = styled.div`
  position: fixed;
  bottom: 1rem;
  left: 1rem;

  button {
    box-shadow: var(--shadow-blur);
    border-radius: 1rem;
    border: 0;
    color: var(--second);
      background: var(--text);
    padding: 0 1rem;
    height: 2rem;
    width: 100%;
    cursor: pointer;
    display: flex;
    gap: .5rem;
    align-items: center;
    justify-content: center;
    &:hover {
        color: var(--text);
      background: var(--card-background);
    }
  }
`

const CreateYourOwn = () => {
  const { isAuthenticated } = useAuth0()
  const leave = useConferenceStore((store) => store.leave)
  const disconnectServer = useConnectionStore((store) => store.disconnectServer)
  const history = useHistory()

  const onEndCall = () => {
    if (window.confirm("You really want to leave the active session?")) {
      leave()
      disconnectServer()
      history.push(`/`)
    }
  }

  if (!isAuthenticated)
    return (
      <Container>
        <button onClick={onEndCall}>
          <div>
            <FiEdit />
          </div>
          <div>
            Start your own <strong>Struggle Session</strong>
          </div>
        </button>
      </Container>
    )
  return <></>
  //   return (
  //     <Container>
  //       <br />
  //       {/* <menu>
  //         <button>
  //           <VscBroadcast /> &nbsp;Broadcast
  //         </button>
  //         <LogoutButton />
  //       </menu> */}
  //     </Container>
  //   )
}

export default CreateYourOwn
