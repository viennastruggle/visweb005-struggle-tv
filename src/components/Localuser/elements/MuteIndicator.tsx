import React from "react"
import styled from "styled-components"
import { ReactComponent as MuteLeaf } from "./../../../assets/mute-leaf.svg"


const Indicator = styled.div`
  position: absolute;
  top: 20px;
  left: 50%;
  width: 73%;
  transform: translateX(-50%);
  display: flex;
  align-items: center;
  justify-content: center;
  svg {
    width: 100%;
  }
`

export const MuteIndicator = () => {
  return (
    <Indicator>
      <MuteLeaf/>
    </Indicator>
  )
}
