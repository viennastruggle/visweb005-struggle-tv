import { useAuth0 } from "@auth0/auth0-react"
import React, { useCallback, useEffect } from "react"
import { InputField } from "../../common/Input/InputField"
import { NameTag } from "../../NameTag/NameTag"
import { useConferenceStore } from "./../../../store/ConferenceStore"

export const NameContainer = () => {
  const [name, setName] = React.useState("Enter Your Name")
  const { user, isAuthenticated, isLoading } = useAuth0()
  const [isActive, setActive] = React.useState(false)
  const setDisplayName = useConferenceStore((store) => store.setDisplayName)
  const displayName = useConferenceStore((store) => store.displayName)
  const onClick = useCallback(() => {
    setActive(true)
  }, [])

  useEffect(() => {
    setName(displayName)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (user) setName(user.name ?? "")
  }, [user])

  const onChange = (e) => {
    setName(e.target.value)
  }

  const onFocusLost = () => {
    setActive(false)
    setDisplayName(name)
  }
  if(isLoading) return <>🤙</>
  if (isActive) {
    return (
      <InputContainer
        autoFocus
        onChange={onChange}
        close={onFocusLost}
        placeholder={name}
      />
    )
  } else {
    return (
      <>
        {isAuthenticated ? (
          <NameTag>✊ &nbsp;{name}</NameTag>
        ) : (
          <NameTag onClick={onClick}>{name}</NameTag>
        )}
      </>
    )
  }
}

const InputContainer = (props) => {
  const handleClose = useCallback(() => {
    props?.close()
  }, [props])

  useEffect(() => {
    document.addEventListener("keyup", (e) => {
      if (e.key === "Escape" || e.key === "Enter") {
        handleClose()
      }
    })
    return document.removeEventListener("keyup", (e) => {
      if (e.key === "Escape" || e.key === "Enter") handleClose()
    })
  }, [handleClose])

  return <InputField {...props} onBlur={handleClose} />
}
