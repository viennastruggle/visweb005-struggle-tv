import styled from "@emotion/styled"
import { ReactNode, ReactNodeArray } from "react"
import PreloadImage from "react-preload-image"
import { Exhibitor, useExhibitorStore } from "./Presets/ExhibitorStore"
import Secret from "./Presets/Secret"

const Container = styled.div`
  position: absolute;
  font-size: 4rem;
  transition: all 0.2s;
  cursor: var(--cursor);

  display: flex;
  flex-direction: column;
  justify-content: center;
  pointer-events: auto;
  flex: 100%;
  z-index: 1;

  & > div {
    flex: 1 0 100%;
  }
  #video {
    pointer-events: auto;
  }

  &:hover {
    transform: scale(1.2);
    opacity: 1 !important;
  }
  h3 {
    font-size: 1.5rem;
    background: white;
    padding: 1rem;
    box-shadow: var(--shadow-blur);
    border-radius: 3rem;
  }
  h3,
  a,
  p {
    color: #000;
    /* text-shadow: var(--shadow-blur); */
    /* color: #ffffff; */
    /* text-shadow: 0px 0px 0 #899cd5, 1px 1px 0 #8194cd, 2px 2px 0 #788bc4,
      3px 3px 0 #6f82bb, 4px 4px 0 #677ab3, 5px 5px 0 #5e71aa, 6px 6px 0 #5568a1,
      7px 7px 0 #4c5f98, 8px 8px 0 #445790, 9px 9px 0 #3b4e87,
      10px 10px 0 #32457e, 11px 11px 0 #2a3d76, 12px 12px 0 #21346d,
      13px 13px 0 #182b64, 14px 14px 0 #0f225b, 15px 15px 0 #071a53,
      16px 16px 0 #02114a, 17px 17px 0 #0b0841, 18px 18px 0 #130039,
      19px 19px 0 #1c0930, 20px 20px 0 #251227, 21px 21px 20px rgba(0, 0, 0, 1),
      21px 21px 1px rgba(0, 0, 0, 0.5), 0px 0px 20px rgba(0, 0, 0, 0.2);
    color: #92a5de; */
    font-size: 2rem;
    margin: 0;
    z-index: 1;
    text-align: center;
    font-weight: normal;
  }
  span {
    text-shadow: var(--shadow-glow);
  }
  a:hover {
    pointer-events: auto;
    text-shadow: var(--shadow-extrude);
    color: #92a5de;
    text-decoration: none;
  }
  img {
  }
  .debug {
    border-radius: 1rem;
    width: 1rem;
    height: 1rem;
    font-size: 1rem;
    overflow: hidden;
    margin: 0 auto;
    &:hover {
      overflow: auto;
      width: auto;
      &:before {
        content: "";
      }
    }
  }
`

type Props = {
  id: string
  exhibitor: Exhibitor
  onClick: () => void
  onMouseEnter: () => void
  onMouseLeave: () => void
  children: ReactNodeArray | ReactNode
}

const Flag = ({ exhibitor, id, children, onClick, onMouseEnter, onMouseLeave }: Props) => {
  const activeFocusGroup = useExhibitorStore((store) => store.activeFocusGroup)
  if (activeFocusGroup)
  if (activeFocusGroup.focusGroupId !== exhibitor.focusGroupId) return <></>


  return (
    <Container
      style={{
        width: exhibitor.sizeW,
        height: exhibitor.sizeH,
        left: exhibitor.positionX,
        top: exhibitor.positionY,
        opacity:
          activeFocusGroup?.focusGroupId === exhibitor.focusGroupId ? 1 : 0.85,
      }}
      onClick={onClick}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
    >
      {exhibitor.avatar !== "" && (
        <PreloadImage
          style={{
            maxWidth: exhibitor.sizeW,
            maxHeight: exhibitor.sizeH,
          }}
          src={exhibitor.avatar}
          alt="Exhibitor"
        />
      )}
      <h3>
        {exhibitor.name}
        <Secret>
          <br />
          <span className="debug">
            X: {exhibitor.positionX}, Y: {exhibitor.positionY}, W:
            {exhibitor.sizeW}, H: {exhibitor.sizeH}
          </span>
        </Secret>
      </h3>
      {children}
    </Container>
  )
}

export default Flag
