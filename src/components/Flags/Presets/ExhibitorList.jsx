import React from "react"
import styled from "styled-components"
import ExhibitorItem from "./ExhibitorItem"
import { useExhibitorStore } from "./ExhibitorStore"

const Container = styled.div`
  max-height: 15rem;
  overflow: auto;
  padding: .5rem;
`

const ExhibitorList = () => {
  const layers = useExhibitorStore((store) => store.layers)
  const activeFocusGroup = useExhibitorStore((store) => store.activeFocusGroup)

  if(!activeFocusGroup) return <></>
  return (
    <Container>
      <div>
        {layers
          ?.find((layer) => layer.id === "exhibitors")
          ?.items.map((item) => (
            <ExhibitorItem
              key={`exhibitorItem-${item.name}-${item.id}`}
              item={item}
            />
          ))}
      </div>
    </Container>
  )
}
export default ExhibitorList
