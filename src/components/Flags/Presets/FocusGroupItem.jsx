import clsx from "clsx"
import React from "react"
import { FiEye, FiEyeOff } from "react-icons/fi"
import styled from "styled-components"
import { useExhibitorStore } from "./ExhibitorStore"
const Container = styled.div`
  cursor: pointer;
  transition: all 0.2s;
  border-top: 1px solid rgba(0,0,0,0.1);
  &:hover {
    opacity: 0.5;
  }
  > div {

    line-height: 2.5rem;
    pointer-events: auto;
    transition: all 0.2s;
    border: none;
    padding: 0 1rem;
    background: none;
    cursor: pointer;
    display: flex;
    align-items: stretch;
    flex-direction: row;
    justify-content: center;;
    > .title {
      flex: 1 0 auto;
    }
    > .indicator {
      flex: 1 0 2rem;
    }
    &.active {
      color: #0000ff;
      font-size: 1rem;

      border: none;
    }
    font-size: 1rem;
    margin: 0;
  }
`

const FocusGroupItem = ({ item }) => {
  const setActiveFocusGroup = useExhibitorStore(
    (store) => store.setActiveFocusGroup,
  )
  const activeFocusGroup = useExhibitorStore((store) => store.activeFocusGroup)
  return (
    <Container>
      <div
        className={clsx({
          active: item.focusGroupId === activeFocusGroup?.focusGroupId,
        })}
        onClick={() => setActiveFocusGroup(item)}
      >
        <div className="title">
        {item.title}
        </div>
        <div className="idicator">
        {item?.focusGroupId === activeFocusGroup?.focusGroupId || !activeFocusGroup ? (
          <FiEye/>
        ) : (
          <FiEyeOff  style={{opacity: 0.1}}/>
          )}
        </div>
      </div>
    </Container>
  )
}
export default FocusGroupItem
