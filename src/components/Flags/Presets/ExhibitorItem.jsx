import clsx from "clsx"
import React from "react"
import { FiEye, FiEyeOff } from "react-icons/fi"
import styled from "styled-components"
import { useExhibitorStore } from "./ExhibitorStore"

const Container = styled.div`
  /* width: 100%; */
  cursor: pointer;
  transition: all 0.2s;
  /* word-wrap: none;
  flex: 1 0 28rem;
  display: flex;
  justify-content: center;
  align-items: center; */
  &:hover {
    opacity: 0.5;
  }
  button {
    line-height: 2rem;
    pointer-events: auto;
    transition: all 0.2s;
    border: none;
    background: none;
    cursor: pointer;
    &.active {
      color: #0000ff;
      font-size: 1rem;

      border: none;
    }
    font-size: 1rem;
    margin: 0;
  }
`

const ExhibitorItem = ({ item }) => {
  const setActiveExhibtor = useExhibitorStore(
    (store) => store.setActiveExhibitor,
  )
  const activeExhibitor = useExhibitorStore((store) => store.activeExhibitor)
  const activeFocusGroup = useExhibitorStore((store) => store.activeFocusGroup)
  if (activeFocusGroup)
    if (activeFocusGroup.focusGroupId !== item.focusGroupId) return <></>
  return (
    <Container>
      <button
        className={clsx({ active: item.name === activeExhibitor?.name })}
        onClick={() => setActiveExhibtor(item)}
      >
        {item?.name === activeExhibitor?.name ||
        !activeFocusGroup ? (
          <FiEye />
        ) : (
          <FiEyeOff />
        )}{" "}
        {item.name}
      </button>
    </Container>
  )
}
export default ExhibitorItem
