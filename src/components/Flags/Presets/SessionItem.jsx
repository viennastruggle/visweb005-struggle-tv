import React from "react";
import ReactMarkdown from "react-markdown";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Container = styled(Link)`
  cursor: var(--cursor);
  box-shadow: -0.25rem 0.25rem 0 var(--color);
  border-radius: 0.5rem;
  background-color: var(--card-background);
  background-image: var(--card-background-image);
  padding: 1rem;
  display: block;
  &:hover {
    box-shadow: -0.4rem 0.4rem 0 var(--color);
    transform: translateX(.2rem) translateY(-.2rem);
  }
  h4 {
    margin: 0;
    font-size: 2rem;
    color: black;
  }
  span {
    font-size: .85rem;
  }
  p {
      font-style: italic;
      color: var(--color);
      margin-bottom: 0.25rem;
  }
  img {
    width: 100%;
    display: block;
    border-radius: .5rem;
  }
`

const SessionItem = ({ item }) => {
  return (
    <Container to={`/session/${item.conferenceName}`}>
      <h4>{item.conferenceName}</h4> <span>
      by {item.hostName}
      </span>
      <ReactMarkdown>
          {item.description}
      </ReactMarkdown>
      {item.image && (<img src={item.image} alt={item.conferenceName}/>)}
    </Container>
  )
}
export default SessionItem
