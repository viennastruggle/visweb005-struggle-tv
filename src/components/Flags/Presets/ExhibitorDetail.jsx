import React from "react"
import ReactMarkdown from "react-markdown"
import styled from "styled-components"

const Container = styled.section`
  p {
    font-size: 1rem;
  }
  border-bottom: 1px solid;
  margin-bottom: 1rem;
`

const ExhibitorDetail = ({ exhibitor }) => {
  return (
    <Container>
      {exhibitor?.image && (
        <img src={exhibitor?.image} alt={exhibitor?.title} />
      )}
      {exhibitor.title && (<h4>{exhibitor?.title}</h4>)}
      <ReactMarkdown>{exhibitor?.description}</ReactMarkdown>
    </Container>
  )
}

export default ExhibitorDetail
