import React, { useState } from "react"
import { FiArrowDownCircle, FiArrowUpCircle, FiX } from "react-icons/fi"
import styled from "styled-components"
import { v4 } from "uuid"
import { useExhibitorStore } from "./ExhibitorStore"
import FocusGroupItem from "./FocusGroupItem"

const Container = styled.div`
  width: 100%;
  overflow: auto;
  max-height: 15rem;
  overflow: auto;
  header {
    button {
      width: 100%;
      display: flex;
      flex-direction: row;
      align-items: stretch;
      div:first-of-type {
        flex: auto;
        text-align: left;
        padding: 0.5rem 1rem;
      }
      div:last-of-type {
        padding: 0.5rem 1rem;
        align-items: center;
        text-align: right;
        display: flex;
        justify-content: flex-end;
      }
    }
  }
  /* white-space: nowrap; */
  button {
    line-height: 2rem;
    pointer-events: auto;
    transition: all 0.2s;
    border: none;
    background: none;
    cursor: pointer;

    &.active {
      color: #0000ff;
      font-size: 1rem;

      border: none;
    }
    font-size: 1rem;
    margin: 0;
  }
`

const ActiveFocusGroup = styled.div`
  margin: 0 1rem 1rem 1rem;
  line-height: 2rem;
  background: white;
  text-align: center;
  border-radius: 1rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 1rem;
`

const FocusGroupList = () => {
  const focusGroups = useExhibitorStore((store) => store.focusGroups)
  const activeFocusGroup = useExhibitorStore((store) => store.activeFocusGroup)
  const setActiveFocusGroup = useExhibitorStore(
    (store) => store.setActiveFocusGroup,
  )
  const [isActive, setIsActive] = useState(false)
  const toggleCollapse = () => {
    setIsActive(!isActive)
  }
  if (!focusGroups) return <></>
  return (
    <Container>
      <header>
        <button onClick={() => toggleCollapse()}>
          <div>Program</div>
          <div>{isActive ? <FiArrowUpCircle /> : <FiArrowDownCircle />}</div>
        </button>
        {activeFocusGroup && (
          <ActiveFocusGroup>
            <div>{activeFocusGroup.title} </div>
            <FiX onClick={() => setActiveFocusGroup(undefined)} />
          </ActiveFocusGroup>
        )}
      </header>
      {isActive && (
        <div>
          {focusGroups.map((item) => (
            <FocusGroupItem key={v4()} item={item} />
          ))}
        </div>
      )}
    </Container>
  )
}
export default FocusGroupList
