import { css, Global } from "@emotion/react"
import { useEffect, useState } from "react"
import useSound from "use-sound"
import { ExhibitorStoreLogic } from "../../Chats/ExhibitorStoreLogic"
import Flag from "../Flag"
import BackgroundImage from "../FlagLayerBackground"
import LayerItem from "../LayerItem"
import campfire from "./../../../assets/sounds/campfire.mp3"
import menuOpen from "./../../../assets/sounds/menu-open.mp3"
import Ducks from "./Ducks"
import { useExhibitorStore } from "./ExhibitorStore"

const Garden = () => {
  const setActiveExhibitor = useExhibitorStore(
    (store) => store.setActiveExhibitor,
  )
  const layers = useExhibitorStore((store) => store.layers)
  const [playOpen] = useSound(menuOpen, { volume: 0.3 })

  const [playCampfire, { stop: stopCampfire }] = useSound(campfire, {
    volume: 0.3,
  })

  const [isHovering, setIsHovering] = useState(false)

  const [background, setBackground] = useState()

  const handleClick = (exhibitor) => {
    playOpen()
    setActiveExhibitor(exhibitor)
  }

  useEffect(() => {
    if (layers) {
      if (layers.find((layer) => layer.id === "background")) {
        const image = layers.find((layer) => layer.id === "background").items[0]
          ?.image
        setBackground(image)
      }
    }
  }, [layers, setBackground])

  return (
    <>
      <Global
        styles={css(`
    @font-face {
      font-family: "AG Book Round Regular";
      src: url("https://res.cloudinary.com/ddddty57j/raw/upload/v1623329489/Fonts/AG_Book_Rounded_Regular_s8qfbr.woff"); /* IE9 Compat Modes */
      src: url("https://res.cloudinary.com/ddddty57j/raw/upload/v1623329489/Fonts/AG_Book_Rounded_Regular_s8qfbr.woff")
          format("woff"); /* Pretty Modern Browsers */
    }
    :root {
      --color: #E2F4E1;
      --background: #3FB835;
      --second: #E2F4E1;
      --third: #E2F4E1;
      --text: black;
    
      --card-background: rgb(226 244 225 / 90%);
      --card-background-image: none;
      --font: "AG Book Round Regular", -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
      "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
      sans-serif;
    }
    `)}
      />
      {background && <BackgroundImage src={background} alt="Background" />}

      {layers
        ?.find((layer) => layer.id === "exhibitors")
        ?.items.map((exhibitor) => {
          return (
            <Flag
              key={`exhibitor-${exhibitor.exhibitorId}-${exhibitor.name}`}
              exhibitor={exhibitor}
              onClick={() => handleClick(exhibitor)}
              onMouseEnter={() => {
                if (exhibitor.exhibitorId === 100) {
                  setIsHovering(true)
                  playCampfire()
                }
              }}
              onMouseLeave={() => {
                if (exhibitor.exhibitorId === 100) {
                  setIsHovering(false)
                  stopCampfire()
                }
              }}
            />
          )
        })}
      {layers
        ?.find((layer) => layer.id === "default")
        ?.items.map((item) => {
          return <LayerItem key={`item-${item.id}-${item.name}`} item={item} />
        })}
      <Ducks />
      <ExhibitorStoreLogic />
    </>
  )
}

export default Garden
