import React from "react"
import styled from "styled-components"
import SessionItem from "./SessionItem"

const Container = styled.section`
  max-width: 900px;
  min-height: 100vh;
  margin: 0 auto;
  padding: 3rem;
  @media screen and (max-width: 800px){
    padding: 1rem;
  }
  & > h3 {
    color: var(--background);
    margin: 0;
    margin-bottom: 2rem;
  }
  & > div {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 1rem;
    @media (max-width: 900px){
      display: flex;
      flex-direction: column;
    }
  }
`

const sessions = [
  {
    conferenceName: "fourthgarden",
    hostName: "Jan Van Eyk Academy, Maastricht",
    description: "digital Open Studios 2021",
    image: "https://res.cloudinary.com/ddqo2y6zh/image/upload/c_scale,q_87,w_600/v1624461543/struggle/fourthgarden/visweb005-title.jpg",
    introduction: ``
  },
  {
    conferenceName: "springfield",
    hostName: "foryouandyourcustomers",
    description: "after work bear",
    introduction: `Join for a casual after work beer and explore the Simpsons capital.`
  },
  {
    conferenceName: "darkroom",
    hostName: "Vienna Struggle",
    description: "dating nightlife networking",
    introduction: `Meet strangers in a laboratory styled dungeon.`
  },
  {
    conferenceName: "curiositytuesday",
    hostName: "Vienna Struggle",
    description: "ideas pitch",
    introduction: `Bring your idea and work. Maybe find partners for your project.`
  },
  {
    conferenceName: "samplescience",
    hostName: "Sample Science",
    description: "lab",
    introduction: `Monthly event for beatmakers & producers from Gretchen, Berlin.`
  },
]

const SessionsList = () => {
  return (
    <Container>
      <h3>Trending</h3>
      <div>
        {sessions.map((item)=>(
          <SessionItem key={`session-item-${item.conferenceName}`} item={item} />
        ))}
      </div>
    </Container>
  )
}
export default SessionsList
