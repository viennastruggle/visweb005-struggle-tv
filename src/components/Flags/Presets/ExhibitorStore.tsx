import { mountStoreDevtool } from "simple-zustand-devtools"
import create from "zustand"

declare global {
  interface Window {
    JitsiMeetJS: any
  }
}

export type Layer = {
  id: string
  items: Exhibitor[]
}

export type FocusGroup = {
  focusGroupId: string
  startDate: string
  endDate: string
  title: string
  // description: string
}

export type Exhibitor = {
  exhibitorId: string
  name: string
  focusGroupId: string
  description: string
  title?: string
  link?: string
  image?: string
  avatar?: string
  positionX: string
  positionY: string
  sizeW: string
  sizeH: string
  layer: string
  isPublished: string
}
export type ExhibitorStore = {
  layers: Layer[] | undefined
  setLayers: (layers:Layer[])=>void
  focusGroups: FocusGroup[] | undefined
  setFocusGroups: (focusgroups:FocusGroup[])=>void
  activeFocusGroup: FocusGroup | undefined
  setActiveFocusGroup: (focusgroup:FocusGroup)=>void
  activeExhibitor: Exhibitor | undefined
  setActiveExhibitor: (exhibitor:Exhibitor)=>void
  error: any
}

export const useExhibitorStore = create<ExhibitorStore>(set => (
  {
    layers: undefined,
    setLayers: (layers:Layer[]) => set({ layers: layers }),
    focusGroups: undefined,
    setFocusGroups: (focusGroup:FocusGroup[]) => set({ focusGroups: focusGroup }),
    activeFocusGroup: undefined,
    setActiveFocusGroup: (focusGroup:FocusGroup) => set({ activeFocusGroup: focusGroup }),
    activeExhibitor: undefined,
    setActiveExhibitor: (exhibitor:Exhibitor) => set({ activeExhibitor: exhibitor }),
    error: undefined,
  })
)
if(process.env.NODE_ENV === 'development') {
  let root = document.createElement('div');
  root.id = 'simple-zustand-devtools-4';
  document.body.appendChild(root);
	mountStoreDevtool('ExhibitorStore', useExhibitorStore, root)
}
