import styled from "@emotion/styled"
import { motion } from "framer-motion"
import { useState } from "react"
import useSound from "use-sound"
import duckQuack from "./../../../assets/sounds/duck-quack.mp3"
import { config } from "./config"

const ducks = {
  RUN_DURATION: 5000, // max
  MARGIN: 100, // distance from edge
}

const Container = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  > div {
    position: absolute;
    height: 300px;
    width: 300px;
    z-index: 10;
  }

  object {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    animation: like-gif steps(28) 1s infinite both;
    pointer-events: auto !important;
    cursor: ne-resize;
  }
`
const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
const getRandomPosition = (min, max) => {
  return { x: getRandomInt(min, max), y: getRandomInt(min, max) }
}
const calcDistance = (x1, y1, x2, y2) => {
  var a = x1 - x2
  var b = y1 - y2
  var c = Math.sqrt(a * a + b * b)
  return c
}

const directions = {
  LEFT: "-1,1",
  RIGHT: "1,1",
}

const Duck = () => {
  const [positionX, setPositionX] = useState(
    getRandomPosition(200, config.canvasWidth - 200).x,
  )
  const [positionY, setPositionY] = useState(
    getRandomPosition(200, config.canvasHeight).y,
  )
  const [direction, setDirection] = useState(directions.RIGHT)
  const [isMoving, setIsMoving] = useState(false)
  const [duration, setDuration] = useState(500)

  const [play] = useSound(duckQuack, { volume: 0.3 })

  // TODO remove
  const move = (x, y, direction, duration) => {
    setPositionX(x)
    setPositionY(y)
    setDirection(direction)
    setDuration(duration)
  }

  const handleMouseEnter = (e) => {
    if (!isMoving) {
      setIsMoving(true)
      play()

      const newX = getRandomPosition(ducks.MARGIN, config.canvasWidth / 2).x
      const newY = getRandomPosition(ducks.MARGIN, config.canvasHeight / 2).y
      const speedModifier =
        calcDistance(newX, newY, positionX, positionY) / config.canvasWidth
      const newDuration = speedModifier * ducks.RUN_DURATION
      move(
        newX,
        newY,
        positionX < newX ? directions.LEFT : directions.RIGHT,
        newDuration,
      )

      setTimeout(() => setIsMoving(false), newDuration)
    }
  }
  return (
    <motion.div
      animate={{ x: positionX, y: positionY }}
      transition={{ duration: duration / 1000 }}
      onMouseEnter={(e) => handleMouseEnter(e)}
      className="ducks"
      width="300"
      height="300"
      alt="Duck"
    >
      <object
        data="../../duck-running-1.svg"
        style={{ transform: `scale(${direction})`, opacity: !isMoving ? 0 : 1 }}
      >
        Duck
      </object>
      <object
        data="../../duck-standing-1.svg"
        style={{ transform: `scale(${direction})`, opacity: isMoving ? 0 : 1 }}
      >
        Duck
      </object>
    </motion.div>
  )
}
const Ducks = () => {
  return (
    <Container>
      <Duck />
      <Duck />
    </Container>
  )
}

export default Ducks
