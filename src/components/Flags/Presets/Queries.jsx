import { useQuery } from "react-query"
export const config = {
  apiURL: "https://visinn001.viennastruggle.at/api",
}

const getUrl = (endpoint, query) => {
  const url = new URL(config.apiURL + endpoint)
  if (query) url.search = new URLSearchParams(query)
  return url
}

export const useGetProjects = (query) => {
  const url = getUrl("/projects", query)
  return useQuery(`getProjects-${query.status}${query.ownerId}`, () =>
    fetch(url).then((res) => res.json()),
  )
}

export const useGetProject = (id) => {
  const url = getUrl("/projects", { id })
  return useQuery(`getProject-${id}`, () =>
    fetch(url).then((res) => res.json()),
  )
}
