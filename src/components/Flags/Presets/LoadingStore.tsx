import { mountStoreDevtool } from "simple-zustand-devtools"
import create from "zustand"

export type LoadingStore = {
  isLoading: boolean
  setIsLoading: (isLoading: boolean) => void
}

export const useLoadingStore = create<LoadingStore>((set) => ({
  isLoading: false,
  setIsLoading: (isLoading: boolean) => set({ isLoading: isLoading }),
}))
if (process.env.NODE_ENV === "development") {
  mountStoreDevtool("LoadingStore", useLoadingStore)
}
