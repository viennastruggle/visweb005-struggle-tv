import styled from "styled-components";
import fourthGardenLogo from "./TheFourthGarden.svg";

const StyledHeader = styled.div`
  position: fixed;
  top: 1rem;
    left: 1rem;
    height: 3rem;
    display: flex;
    width: 15rem;
    display: flex;
    align-items: center;
    justify-content: center;
  background: var(--card-background);
  border-radius: 1.5rem;
  box-shadow: var(--shadow-blur);
  z-index: 10000;
  text-decoration: none;
  color:#000;
  &:hover {
    opacity: .75;
  }
  svg {
      width: 8rem;
  }
`

export const GardenHeader = () => (
  <StyledHeader><img src={fourthGardenLogo} alt="Logo"/></StyledHeader>
)
