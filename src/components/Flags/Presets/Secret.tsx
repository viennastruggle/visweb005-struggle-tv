import React, { FunctionComponent } from "react"

function isQueryParamSet(param: string) {
  const params = new URLSearchParams(window.location.search)
  const value = params.get(param)
  return value === "true" || value === "1"
}

export const isSecret: Function = () => {
  return isQueryParamSet("secret")
}

export const Secret: FunctionComponent = ({ children }) => {
  return <>{isSecret() && children}</>
}
export const Unsecret: FunctionComponent = ({ children }) => {
  return <>{!isSecret() && children}</>
}

export default Secret
