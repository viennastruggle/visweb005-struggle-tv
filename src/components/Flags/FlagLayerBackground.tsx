import styled from "@emotion/styled";
import PreloadImage from 'react-preload-image';

const BackgroundImage = styled(PreloadImage)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 0;
  display: flex;
`
export default BackgroundImage;