import styled from "@emotion/styled"
import React from "react"
import { useConferenceStore } from "../../store/ConferenceStore"
import Garden from "../Flags/Presets/Garden"
import ForYourBeer from "./Presets/ForYourBeer"

const Container = styled.div`
  position: relative;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  pointer-events: none;
  /* &::after {
    background: url('data:image/svg+xml;utf8,<svg height="300" width="1" xmlns="http://www.w3.org/2000/svg"><rect width="1" height="0.5" style="fill:rgba(255, 0, 0,1)" /></svg>') repeat top left;
    content: "";
    display: block;
    pointer-events: none;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 9999;
    position: absolute;
} */
`

const FlagLayer: React.FC = () => {
  const { conferenceName } = useConferenceStore();
  return (
    <Container>
      {conferenceName === "fourthgarden" && <Garden />}
      {conferenceName === "foryourbeer" && <ForYourBeer />}
    </Container>
  )
}

export default FlagLayer
