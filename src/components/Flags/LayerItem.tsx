import styled from "@emotion/styled"
import { ReactNode, ReactNodeArray } from "react"
import PreloadImage from "react-preload-image"
import { Exhibitor } from "./Presets/ExhibitorStore"
import Secret from "./Presets/Secret"

const Container = styled.div`
  position: absolute;
  font-size: 4rem;
  transition: all 0.5s;

  display: flex;
  flex-direction: column;
  justify-content: center;
  pointer-events: auto;
  flex: 100%;
  z-index: 1;
  & > div {
    flex: 1 0 100%;
  }
  #video {
    pointer-events: auto;
  }

  h3,
  a,
  p {
    color: #000;
    text-shadow: var(--shadow-blur);
    font-size: 2rem;
    margin: 0;
    z-index: 1;
  }
  span {
    text-shadow: var(--shadow-glow);
  }
  a:hover {
    pointer-events: auto;
    text-shadow: var(--shadow-extrude);
    color: #92a5de;
    text-decoration: none;
  }
  img {
  }
  .debug {
    border-radius: 1rem;
    width: 1rem;
    height: 1rem;
    font-size: 1rem;
    overflow: hidden;
    margin: 0 auto;
    &:hover {
      overflow: auto;
      width: auto;
      &:before {
        content: "";
      }
    }
  }
`

type Props = {
  id: string
  item: Exhibitor
  onClick: () => void
  children: ReactNodeArray | ReactNode
}

const LayerItem = ({ item, children, onClick }: Props) => {
  return (
    <Container
      style={{
        width: item.sizeW,
        height: item.sizeH,
        left: item.positionX,
        top: item.positionY,
      }}
    >
      {item.image !== "" && (
        <PreloadImage
          style={{
            maxWidth: item.sizeW,
            maxHeight: item.sizeH,
          }}
          src={item.image}
          alt="Layer Item"
        />
      )}
      <h3>
        <Secret>
          <span className="debug">
            X: {item.positionX}, Y: {item.positionY}, W:
            {item.sizeW}, H: {item.sizeH}
          </span>
        </Secret>
      </h3>
      {children}
    </Container>
  )
}

export default LayerItem
