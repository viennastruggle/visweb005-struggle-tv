import React from "react"
import { FiUser } from "react-icons/fi"
import styled from "styled-components"
import { useLocalStore } from "../../store/LocalStore"
const Container = styled.div`
  cursor: pointer;
  transition: all 0.2s;
  border-top: 1px solid rgba(0, 0, 0, 0.1);
  &:hover {
    opacity: 0.5;
  }
  > div {
    line-height: 2.5rem;
    pointer-events: auto;
    transition: all 0.2s;
    border: none;
    padding: 0 1rem;
    background: none;
    cursor: pointer;
    display: flex;
    align-items: stretch;
    flex-direction: row;
    justify-content: center;
    > .title {
      flex: 1 0 auto;
    }
    > .indicator {
      flex: 1 0 2rem;
    }
    &.active {
      color: #0000ff;
      font-size: 1rem;

      border: none;
    }
    font-size: 1rem;
    margin: 0;
  }
`

const ParticipantItem = ({ item }) => {
  const { changePan } = useLocalStore()
  const panToParticipant = (pos) => {
    changePan(pos)
  }
  return (
    <Container>
      <div
        onClick={() => panToParticipant(item[1].user?.pos)}
        key={item[0]}
      >
        <div className="title"><FiUser/> {item[1].user?._displayName || item[0]}</div>
        <div className="idicator">
          {/* <FiEye /> */}
        </div>
      </div>
    </Container>
  )
}
export default ParticipantItem
