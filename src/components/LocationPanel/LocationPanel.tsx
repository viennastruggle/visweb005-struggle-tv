import React, { useState } from "react"
import { FiArrowDownCircle, FiArrowUpCircle } from "react-icons/fi"
import styled from "styled-components"
import { useConferenceStore } from "../../store/ConferenceStore"
import MiniMap from "./MiniMap"
import ParticipantList from "./ParticipantList"

const Container = styled.div`
  header {
    button {
      width: 100%;
      display: flex;
      flex-direction: row;
      align-items: stretch;
      div:first-of-type {
        flex: auto;
        text-align: left;
        padding: 0.5rem 1rem;
      }
      div:last-of-type {
        padding: 0.5rem 1rem;
        align-items: center;
        text-align: right;
        display: flex;
        justify-content: flex-end;
      }
    }
  }
  button {
    line-height: 2rem;
    pointer-events: auto;
    transition: all 0.2s;
    border: none;
    background: none;
    cursor: pointer;

    &.active {
      color: #0000ff;
      font-size: 1rem;

      border: none;
    }
    font-size: 1rem;
    margin: 0;
  }
`

export const LocationPanel = () => {
  const [isCollapsed, setIsCollapsed] = useState(true)
  const count = Object.entries(useConferenceStore().users).length
  return (
    <Container>
      <header>
        <button onClick={() => setIsCollapsed(!isCollapsed)}>
          <div>Who is online? {count > 0 && <>({count})</>}</div>
          <div>{isCollapsed ? <FiArrowUpCircle /> : <FiArrowDownCircle />}</div>
        </button>
      </header>

      {!isCollapsed && (
        <>
          <MiniMap />
          <ParticipantList />
        </>
      )}
    </Container>
  )
}
