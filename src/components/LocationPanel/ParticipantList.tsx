import { useAuth0 } from "@auth0/auth0-react"
import React from "react"
import { FiUser } from "react-icons/fi"
import styled from "styled-components"
import { useConferenceStore } from "../../store/ConferenceStore"
import ParticipantItem from "./ParticipantItem"

const Container = styled.div`
  max-height: 15rem;
  overflow: auto;
  & > div:first-of-type {
    padding: 0 1rem;
    line-height: 2rem;
  }
`

const ParticipantList = () => {
  const {user, isAuthenticated} = useAuth0()
  const users = useConferenceStore(store => store.users)
  const displayName = useConferenceStore(store => store.displayName);
  return (
    <Container>
      <div style={{ pointerEvents: "none", color: "blue" }}>
        <FiUser /> {isAuthenticated ? user?.name :displayName}
      </div>
      {Object.entries(users).map((user) => {
        return <ParticipantItem item={user} />
      })}
    </Container>
  )
}

export default ParticipantList
