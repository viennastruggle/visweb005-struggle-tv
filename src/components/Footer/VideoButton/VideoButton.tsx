import * as React from 'react'
import { useLocalStore } from '../../../store/LocalStore'
import { Button } from '../../common/Buttons/Button'

export const VideoButton = () => {
	const toggleMuteVideo = useLocalStore(store => store.toggleMuteVideo)
	const muteVideo = useLocalStore(store => store.muteVideo)
  
	if(muteVideo) {
	  return <Button type="danger" onClick={toggleMuteVideo}>Enable Video</Button>
	} else {
	  return <Button type="secondary" onClick={toggleMuteVideo}>Disable Video</Button>
	}
}
