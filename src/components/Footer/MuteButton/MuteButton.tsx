import React from 'react';
import { useLocalStore } from './../../../store/LocalStore';
import { Button } from './../../common/Buttons/Button';

export const MuteButton = () => {

  const toggleMute = useLocalStore(store => store.toggleMute)
  const mute = useLocalStore(store => store.mute)

  if(mute) {
    return <Button type="danger" onClick={toggleMute}>Unmute</Button>
  } else {
    return <Button type="secondary" onClick={toggleMute}>Mute</Button>
  }
}
