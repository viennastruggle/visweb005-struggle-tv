import * as React from "react"
import { useHistory } from "react-router-dom"
import { useConferenceStore } from "../../../store/ConferenceStore"
import { useConnectionStore } from "../../../store/ConnectionStore"
import { Button } from "../../common/Buttons/Button"

export const JoinButton = ({ joined = false }) => {
  const leave = useConferenceStore((store) => store.leave)
  const disconnectServer = useConnectionStore((store) => store.disconnectServer)
  const conferenceName = useConferenceStore((store) => store.conferenceName)
  const history = useHistory()

  const onEndCall = () => {
    if (window.confirm("You really want to leave the active session?")) {
      leave()
      disconnectServer()
      history.push(`/`)
    }
  }

  const onStartCall = (e) => {
    e.preventDefault()
    //perhaps it is better to create a connection and then forward to "session/" page?
    history.push(`/session/${conferenceName}`)
  }

  if (joined) {
    return (
      <Button type="danger" onClick={onEndCall}>
        Leave Call
      </Button>
    )
  } else {
    return (
      <Button type="primary" onClick={onStartCall}>
        Join
      </Button>
    )
  }
}
