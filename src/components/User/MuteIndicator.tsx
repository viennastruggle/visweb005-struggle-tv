import * as React from "react"
import styled from "styled-components"
import { ReactComponent as MuteLeaf } from "./../../assets/mute-leaf.svg"

const MuteContainer = styled.div`
  position: absolute;
  top: 170px;
  left: 50%;
  transform: translateX(-50%);
  font-size: 2.5rem;
  text-align: center;
  svg {
    width: 40%;
  }
`

export const MuteIndicator = () => (
  <MuteContainer>
    {/* <MuteCatSmall /> */}
    <MuteLeaf />
  </MuteContainer>
)
