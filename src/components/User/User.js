import React, { useCallback, useEffect } from "react"
import { useConferenceStore } from "./../../store/ConferenceStore"
import { ReloadHint } from "../ReloadHint/ReloadHint"
import { AudioTrack } from "./AudioTrack"
import { MuteIndicator } from "./MuteIndicator"
import { VideoTrack } from "./VideoTrack"
import { NameTag } from "../NameTag/NameTag"
import styled from "styled-components"
import { FiVideoOff } from "react-icons/fi"

const Volume = styled.div`
  text-align: center;
`

const DisabledVideo = styled.div`
  position: absolute;
  top: 0;
  z-index: -1;
  width: 200px;
  height: 200px;
  border-radius: 50%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: var(--background);
  color: white;
  font-weight: bold;
  & svg {
    margin: 5px;
  }
`

export const User = ({ id, user }) => {
  const userStore = useConferenceStore(
    useCallback((store) => store.users[id], [id]),
  )
  console.log(userStore)
  const myPos = useConferenceStore(
    useCallback((store) => store.users[id]["pos"], [id]),
  )
  const myVolume = useConferenceStore(
    useCallback((store) => store.users[id]["volume"], [id]),
  )
  const isMute = useConferenceStore(
    useCallback((store) => store.users[id]["mute"], [id]),
  )
  const isMuteVideo = useConferenceStore(
    useCallback((store) => store.users[id]["muteVideo"], [id]),
  )
  const calculateVolume = useConferenceStore(
    useCallback((store) => store.calculateVolume, []),
  )
  useEffect(() => {
    calculateVolume(id)
  }, [id, calculateVolume, myPos])

  return (
    <div
      style={{
        position: "absolute",
        left: `${myPos.x}px`,
        top: `${myPos.y}px`,
        zIndex: 1,
      }}
      className="userContainer"
    >
      {!isMuteVideo ? (
        <VideoTrack id={id} />
      ) : (
        <DisabledVideo>
          <FiVideoOff />
        </DisabledVideo>
      )}
      <ReloadHint />
      <AudioTrack id={id} volume={myVolume} />
      <NameTag>
        {user?.user?._displayName || "Friendly Sphere"}
        {isMuteVideo && (
          <>
            {" "}
            <FiVideoOff />
          </>
        )}
      </NameTag>
      <Volume>Volume {Math.round(myVolume * 11)}</Volume>
      {isMute && <MuteIndicator>🤭</MuteIndicator>}
    </div>
  )
}
