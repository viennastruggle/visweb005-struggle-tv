import React, { useEffect, useState } from "react"
import { TransformComponent, TransformWrapper } from "react-zoom-pan-pinch"
import { useLocalStore } from "./../../store/LocalStore"
import { panOptions, transformWrapperOptions } from './panOptions'

const panChange = store => store.onPanChange
const setPos = store => store.setLocalPosition

export const PanWrapper = ({children}) => {

  const onPanChange = useLocalStore(panChange)
  const setLocalPosition = useLocalStore(setPos)

  const [isMounted, setIsMounted] = useState(false)
  useEffect(() => {
    if(!isMounted){
      console.log("defaultPosition:",{x:transformWrapperOptions.defaultPositionX,y:transformWrapperOptions.defaultPositionY})
      onPanChange({scale:transformWrapperOptions.scale,positionX:transformWrapperOptions.defaultPositionX,positionY:transformWrapperOptions.defaultPositionY})
      setLocalPosition(panOptions.user.initialPosition)
      setIsMounted(true);
    }
  },[onPanChange, setLocalPosition, setIsMounted, isMounted])

  return (
    <TransformWrapper 
      {...transformWrapperOptions}
      onZoomChange={onPanChange}
      onPanning={onPanChange}
      onPinchingStop={onPanChange}
    >
      <TransformComponent>
        {children}
      </TransformComponent>
    </TransformWrapper>
  )
}