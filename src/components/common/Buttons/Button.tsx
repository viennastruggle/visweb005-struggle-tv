import styled, { css } from 'styled-components';
interface IButton {
  type?: any
  danger?: boolean
}

export const Button = styled.button<IButton>`
  font-size: 1rem;
  display: flex;
	flex-direction: row;
	align-items: center;
  justify-content: center;
	height: 2rem;
  padding: 0 2rem;
  border: none;
  border-radius: 1rem; 
  color: ${props => props.theme.primary['1']};
  /* border: 1px solid ${props => props.theme.primary['1']}; */
  background-color: ${props => props.theme.base['5']};
  /* font-weight: bold; */
  box-shadow: var(--shadow-blur);
  cursor: pointer;

  ${props => props.type === "primary" && css`
    color: ${props => props.theme.base['5']};
    /* border: 1px solid ${props => props.theme.primary['2']}; */
    background-color: ${props => props.theme.primary['2']};
  `}

  ${props => props.type === "danger" && css`
    color: var(--text);
    /* border: 2px ${props => props.theme.primary['1']}; */
    background-color: var(--color);
    & svg {
      fill: var(--text);
    }
  `}

  & svg {
    margin-right: 5px;
  }

  &:hover {
    background-color: ${props => props.theme.base['4']};

    ${props => props.type === "primary" && css`
    /* border: 1px solid ${props => props.theme.primary['1']}; */
    background-color: ${props => props.theme.primary['3']};
    `}

    ${props => props.type === "danger" && css`
      /* border: 1px solid ${props => props.theme.secondary['2']}; */
      background-color: ${props => props.theme.secondary['2']};
    `}
  
  }
  &:active {
      background-color: ${props => props.theme.primary['4']};

    ${props => props.type === "primary" && css`
      background-color: ${props => props.theme.primary['1']};
    `}

    ${props => props.type === "danger" && css`
      background-color: ${props => props.theme.secondary['1']};
    `}
  }
  
  &:focus {
    outline: none;
  }
`

