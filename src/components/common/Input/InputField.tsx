import styled from "styled-components";

export const InputField:any = styled.input`
  margin-top: .25rem;
  border-radius: 1rem;
  font-weight: 500;
  color: var(--text);
  background: white;
  border: 1px solid var(--card-background);
  text-align: center;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 2rem;
  width: 100%;

  &:focus {
    outline: none;
    border: 1px solid var(--text);
  }
`;
