import * as React from "react"
import { IoMdClose } from "react-icons/io"
import styled from "styled-components"
import { useInfoStore } from "./InfoStore"

const InfoBox = styled.div`
  user-select: none;
  padding: 1rem;
  position: relative;
  font-size: 0.85rem;
  line-height: 1rem;
  font-weight: normal;
  background-color: #fff;
  border-radius: 0;
  transition: all 0.5s;
  z-index: 10001;
  color: #555;
  text-align: center;
  b {
    color: #000;
  }
  &:hover {
    background-color: #fefefe;
    /* box-shadow: var(--shadow-blur); */
  }
`

const StyledClose = styled(IoMdClose)`
  position: absolute;
  right: 5px;
  top: 5px;
`

export const Info = ({ children }) => {
  // const [ visible, toggleVisible ] = React.useState(true)
  const visible = useInfoStore((store) => store.show)
  const setHidden = useInfoStore((store) => store.setHidden)

  if (visible) {
    return (
      <div onClick={setHidden}>
        <InfoBox>
          {children}
          <StyledClose />
        </InfoBox>
      </div>
    )
  } else {
    return null
  }
}
