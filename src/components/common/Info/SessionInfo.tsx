import * as React from "react"
import { IoMdClose } from "react-icons/io"
import styled from "styled-components"
import { useInfoStore } from "./InfoStore"

const InfoBox = styled.div`
  --info-width: 20rem;
  user-select: none;
  padding: 3rem;
  position: fixed;
  top: 50vh;
  left: 50vw;
  width: var(--info-width);
  transform: translate(-50%, -50%);
  margin: auto 0;
  font-size: 1rem;
  line-height: 1.1rem;
  font-weight: normal;
  background-color: var(--card-background);
  border-radius: 1rem;
  transition: all 0.5s;
  z-index: 10001;
  box-shadow: var(--shadow-blur);
  b {
    color: #000;
  }
  &:hover {
  }
`

const StyledClose = styled(IoMdClose)`
  position: absolute;
  right: 0.5rem;
  top: 0.5rem;
  font-size: 1.5rem;
`

export const SessionInfo = ({ children }) => {
  const visible = useInfoStore((store) => store.show)
  const setHidden = useInfoStore((store) => store.setHidden)

  if (visible) {
    return (
      <div onClick={setHidden}>
        <InfoBox>
          {children}
          <StyledClose />
        </InfoBox>
      </div>
    )
  } else {
    return null
  }
}
