import styled from "styled-components"

export const BigHeadline = styled.h1`
  font-size: 1.5rem;
  font-weight: 500;
  color: black;
  margin: 0;
  text-align: left;
  width: 100%;
  margin-bottom: 0.25rem;
`
