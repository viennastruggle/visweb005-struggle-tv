import styled from "styled-components"

export const SubHeadline = styled.h3`
  font-weight: 400;
  font-size: 1rem;
  margin: 0;
`
