import styled from "styled-components";
import ViennaStruggleLogo from './ViennaStruggleLogo';

const StyledHeader = styled.a`
  position: fixed;
  top: 1rem;
  right: 0rem;
  z-index: 10000;
  text-decoration: none;
  color:#000;
  &:hover {
    opacity: .75;
  }
`

export const Header = ({children}) => (
  <StyledHeader href="/"><ViennaStruggleLogo/></StyledHeader>
)
