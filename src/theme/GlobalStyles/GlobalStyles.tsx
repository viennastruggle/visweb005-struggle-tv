import { createGlobalStyle } from "styled-components"
import { ITheme } from "./../theme"

export const GlobalStyles = createGlobalStyle<{ theme: ITheme }>`

:root {
  --sidebar-width: 15rem;
  --header-height: 8rem;
  --header-width: 20rem;
  --content-width: min(calc(100% - 4rem), 72rem);
  --form-width: 30rem;
  --content-height: calc(100vh - var(--header-height));

  --color: #bb6bd9;
  --background: #ffff00;
  --second: #0000ff;
  --third: #00ff00;
  --text: black;

  --chat-incoming-message-background: #ffa800;
  --chat-outgoing-message-background: #ffc3d5;

  --card-background: #d9d9d9;
  --card-background-image: linear-gradient(315deg, #d9d9d9 0%, #f6f2f2 74%);

  --font: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;

  --shadow: 1rem 1rem rgba(255,255,255,.5);
  --shadow-blur: 0px 4px 4px rgba(0, 0, 0, 0.25);
  --shadow-glow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #49ff18,
      0 0 30px #49ff18, 0 0 40px #49ff18, 0 0 55px #49ff18, 0 0 75px #49ff18;
  --shadow-extrude: 0px 0px 0 #899cd5, 1px 1px 0 #8194cd, 2px 2px 0 #788bc4,
      3px 3px 0 #6f82bb, 4px 4px 0 #677ab3, 5px 5px 0 #5e71aa, 6px 6px 0 #5568a1,
      7px 7px 0 #4c5f98, 8px 8px 0 #445790, 9px 9px 0 #3b4e87,
      10px 10px 0 #32457e, 11px 11px 0 #2a3d76, 12px 12px 0 #21346d,
      13px 13px 0 #182b64, 14px 14px 0 #0f225b, 15px 15px 0 #071a53,
      16px 16px 0 #02114a, 17px 17px 0 #0b0841, 18px 18px 0 #130039,
      19px 19px 0 #1c0930, 20px 20px 0 #251227, 21px 21px 20px rgba(0, 0, 0, 1),
      21px 21px 1px rgba(0, 0, 0, 0.5), 0px 0px 20px rgba(0, 0, 0, 0.2);
  --cursor: pointer;
  --cursor-asterix: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAJFklEQVR42rWXCXBU9R3Hv+/Ye7PZTbLZJCQBRIej2JHSkStgoS2jWJlBzhpNOKscBR2wIrSlVA4NIGoJMBVBoTOFloKlDGEIV0K4hyvDCBEQAiSQY7PZ7G52913//t4L4WiCoh3ezl5v3/v/Pr/f//s7lsN9h8fjcdpstmcFnq9rjkYrOY6L1NfXq3iMB3f/F7fbnZGamrqtS5cnfnL7dk1JdXV1SSwWKzObTRV1dfW3HjuA3W7J8KZmbFmw/KOcZ7pkYf++Azh69AiruFhxrPpWdVE8Ht9vtVrL/X5/6PEAWO2+5BT3P976YNWg/LEjkCQAtAU4d+4sjh09hrLDhwPnz58vbmxs/JLn+ZKmpqbq/xsgi8uxArxFYXI4yF9JTe7Ab576x2WDeg38OXqlJ8Lnst+9+Nq1azhz5gz27d+vHC4rO3b16tXdpJedDYHAuR8MkMn1d9Fbqsa0UEyo89p9sU/nLFrSt8+QYWiONqN3tg+JdjPYfeGKRCK4fOUKSkpKULRr16Uzp08fjkWjfwuGQvt+CEACA5/GGIvJQtBnTmlc9faihX2GvTwW9cEQBDL9TFYqRF4AQYIyAwLfgqIxhpqa26STY9i+bXvdkSOHT/gb/BtUWf13OBJWHgmgAzcggd58LQCNXlNKYPWs38/rO2JcPmRZQigag8tmRbe0JAOAsXs3kw5whwXNzc2klXPYtGlT8969e8tramoKnU7nVsqk2LcD8P0TwPg7AEGvmOQvnDb37X5jXpsMWZGhqSqisop0twNZngSoqgb2v4tQVHgi0Vk0jeHEiePYuHEjKy0tPUgAK0VRLK6rq2sXhLYgh7YABoAiBlN4d33hlNlv9s+dOBWKqhCAZnguaxo6p7iR7LC2C3EvKgRDQPrvBw8cxOefb2DFxcVrSTfvUda0qSVcFj/IqWmaj5aUCMDDu+oKJ8yanpP/xiyoigJVUw3PZDKqh7yrzwObWSQ47Vv3VhB4475QKIQPP1yJDRvW7wlHIpP89fU3HwDI5gY4VSMCIICmROa8vSpvxhvPTZoxh8Kpkbdyi2fklb4VdjKuQ+hCVDX2UABdK3QLRAKpq/dj+EsvSZe+rnjV39DwzwcjwD3r1GDxgWmyJISczHnrL+Mmjx8ydfa7xt4qinJnn2lReoRjCpIcNoJwG1mgsfYhdMP6cf36daz7bB02b95cVnWzaiyJ9YHixXUU+jpkTUzjGJMlPmTXnLc/eTlv9C9nzv0ThVE0hHj3Yt0zegaaJXRKSkDHFFfbrSBS8U5q7NixA+vXr8ep06fOUvWcEA6Fz7bRQCe+n0NiQhrPoMTRZNZcNStfGPXii7MXLIbFYjNSscU4Z0RA3wrdqD8SQ/f0ZGRQdrRCtKblhYsXsaZwNUpKS0B9Y08gEJhJnle0mwU+5NjNHEvXGKdS1nPMVftBztD+o+ctWYkElwuSAdDqewuGQBCBWNzYjt7ZqUhJsBmLkZcU6i04VFqKyuuVuF55Yx+l38hYPBp8mFa4NOTYBI5l0LoE0Mw4d+3Cp/t0z1+4Yg2SvamQJemesO6D0D9VB8OwWaz4aWYSvqKGtWXrVmRnZyM3N5ckxTBz5szKnTt3jg6Fmk4+FCAT/W2M4wiAYzIicd7TMLdz9/QZC1YUolOXpyDF4w+q+04F0GMS0zjUNoVxdNeXiNZWY9KE8ejxox53+0Z5eTny8vKOkxCH0jY0PQzASgBp5JcpzqIhwR2Y6s2yzV+wfJXQs1dvxOP3Clir71S0YLPZ0Uxw69cWIhgMYuL0tzCwayZIzEZ6tvaMpUuXqgUFBX+g7VnaLkAGBljo2nTeAIgFhcSmXzu8yuJ5i5c5+g8ZSgBRtJY9HUAvTHa7wzi17qMCNIQiGPn6m+ApY5502/AkpWdrpdRT8UJFBcaMGnW6qqpqcHtR0JuRid4zaHGzwqQgczT9zJoc+XjGO/PTho/JRTwWM7xuNe5wOI3FVxcsQmXlDUx6989wJ7ogU+t22S3o2SEFZkGgazUDgMov8vPzbx06dGgkZcTRtmnI9RNl8OlkwKYyNaxagp1FT+CzMfnju74+ey4USW7pghRWZ4KTIiJh9bLFOFi8G7OXrUbPnk/DxasUbh7BqIRMali+RLsBoJ/TS/HkyZP9RUVFE+jzf9oAZKGPoHGirgGHXo7jXKPZ6gut7dG7x+DFn/wVdvJYkWU4nQkI+OuxZsX72LNjGzI6PoGFa77AUx18oKZhiC4iqYhT9+zidcNtMxlFqeLSZbyW+0otCTGXWvTedkTYh+N4kSYiJNJXJcbCUUda83y7m02bMvMdbsSreSQsDV9f+Aprlr+P8lPHYXM4qFGq4rARY/DbOb+jAiRQyZYNATZGZUjkvcdJBYpqyOrlS7Br+9ZL9NPzNNJ9004EBujwSZRRyRQFTWJSBI7AwJRsodDudKb8atQ4WEnxO7f+HTW3bsLEO8oDtbG19kRhuMmqPf+LF4bjlYlTkOpLgyiajC4UpiJ15epV/OuL9ThZdgA02n9K8+Nv2s0C/SWL6+eiZptqpBn1lxgaeUeaND0hWciPxpo9+nmT2eJXouLuULXwsSoJ3zBTuJsnk3+PM8mDU7w+dOvxY3gJQqHuWV9Tg0sUsQa/HxzPH6utrc1raGi49FAAmgttpPM0vXvCCLiqxVmTYEqUBjvc4lAaMdRoI3ZJQUuxCTYmcLyTaobevn2udEyjSAyT5bi3pQfrT54ywHJTlpWiSCRcQKP95YdWQv0lFQNFE6+mUzW00Ql98tRVT6WZchCKlUqKxMEcMcHkIQN6nDX9VpUaaBwhkylBGWBN4PuYzBwNt6TDqHBDFkO7q6orD+A7jrt/TDK5vh4G0Xun6rCWCU8fArQw9cAAOUW+MS9NKVaqcrqvxjU0D9DEIMUYZJGusNF8SedFfy1OBr7L+AMAejoyTkwiI/r/BOq6TNEYHxHABW+wQ0ZD6MDrf2JYCjG2tD8j5i2jF/TZxCjSkEwQ/JUojX0vABjlcABHPckmMt6kUEJwjI9Xs7IHJg7Si4nucpP/DjImoLVXUwsg6AhjYqjqEY23AXjUI417jqd4m8BkC8czXtN4KgKQSb7yTRxh32et/wJPSoRd6oGs9QAAAABJRU5ErkJggg=="), auto;
  --cursor-flower: url("data:image/svg+xml,%3Csvg width='40' height='40' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill='%23e40986' d='M38.4 8.8c-1.5-1-5.5-1.1-6.6-1.1-.1 0-.2.1-.1.2.2.4.5 1.3.4 2-.2 1.5-.8 2.2-2.7 1.6-1.2-.4-1.7-1.1-2-2.1-.5-1.6 0-2.4 1.7-2.5.5 0 1.4.3 1.9.4.1 0 .3 0 .3-.2.1-1.2-.2-5.3-1-6.3C18.4 0 16.8 16 18.8 19.7c5 2 19.9 2.2 19.6-10.9z'/%3E%3Cpath fill='%23ee7e26' d='M31.4 10.1c-.1-.6-.4-1.4-.8-1.8-2-1.9-3.3-.7-3.3 1.8-.2.3.3 1.3 1.5 1.4.6.1 2.8-.5 2.6-1.4z'/%3E%3Cellipse transform='rotate(-45.001 29.461 9.19)' fill='%23f7ec17' cx='29.5' cy='9.2' rx='1' ry='1.9'/%3E%3Cpath fill='%234d887c' d='M27.3 10.1c-1.8 2.8-5.4 6-5.4 6l.9.8s3.7-4.6 6-5.5c-1.2-.1-1.1-.7-1.5-1.3z'/%3E%3Cpath fill='%23fff' opacity='.2' d='M38.4 8.8s-4 10.6-14.2 11l-5.5.2c19.1 4.2 20.1-9.2 19.7-11.2z'/%3E%3Cpath fill='%239f235e' d='M30.2.7s-5.9 1.9-8.3 5.8c-1.8 3-3.4 8.2-3.4 8.2-.8 1.5-.7-4.8 2.2-9.4C23.6.7 28.6.6 28.6.6l1.6.1z'/%3E%3Cpath fill='%23e40986' d='M.1 31.5c1.5 1 5.5 1.1 6.6 1.1.1 0 .2-.1.1-.2-.2-.4-.5-1.3-.4-2 .2-1.5.8-2.2 2.7-1.6 1.2.4 1.7 1.1 2 2.1.5 1.6 0 2.4-1.7 2.5-.4.1-1.4-.2-1.8-.4-.1 0-.3 0-.3.2-.1 1.2.2 5.3 1 6.3 11.8.7 13.4-15.3 11.4-19-5.1-1.9-19.9-2.1-19.6 11z'/%3E%3Cpath fill='%23ee7e26' d='M7.1 30.2c.1.6.4 1.4.8 1.8 2 1.9 3.3.7 3.3-1.8.2-.3-.3-1.3-1.5-1.4-.6-.1-2.8.5-2.6 1.4z'/%3E%3Cellipse transform='rotate(-45.001 8.988 31.086)' fill='%23f7ec17' cx='9' cy='31.1' rx='1' ry='1.9'/%3E%3Cpath fill='%234d887c' d='M11.1 30.2c1.8-2.8 5.4-6 5.4-6l-.9-.8s-3.7 4.6-6 5.5c1.2.1 1.2.7 1.5 1.3z'/%3E%3Cpath fill='%23fff' opacity='.2' d='M.1 31.5s4-10.6 14.2-11l5.5-.2C.7 16.1-.4 29.5.1 31.5z'/%3E%3Cpath fill='%239f235e' d='M8.3 39.5s5.9-1.9 8.3-5.8c1.8-3 3.4-8.2 3.4-8.2.8-1.5.7 4.8-2.2 9.4-2.9 4.6-7.9 4.7-7.9 4.7-.1.1-1.7 0-1.6-.1z'/%3E%3Cg%3E%3Cpath fill='%23e40986' d='M30.3 39.7c1-1.5 1.1-5.5 1.1-6.6 0-.1-.1-.2-.2-.1-.4.2-1.3.5-2 .4-1.5-.2-2.2-.8-1.6-2.7.4-1.2 1.1-1.7 2.1-2 1.6-.5 2.4 0 2.5 1.7 0 .5-.3 1.4-.4 1.9 0 .1 0 .3.2.3 1.2.1 5.3-.2 6.3-1 .7-11.8-15.3-13.4-19-11.4-1.9 5-2.1 19.8 11 19.5z'/%3E%3Cpath fill='%23ee7e26' d='M29 32.7c.6-.1 1.4-.4 1.8-.8 1.9-2 .7-3.3-1.8-3.3-.3-.2-1.3.3-1.4 1.5-.1.6.5 2.8 1.4 2.6z'/%3E%3Cellipse transform='rotate(-45.001 29.885 30.803)' fill='%23f7ec17' cx='30' cy='31' rx='1.9' ry='1'/%3E%3Cpath fill='%234d887c' d='M29 28.7c-2.8-1.8-6-5.4-6-5.4l-.8.9s4.6 3.7 5.5 6c.1-1.2.7-1.2 1.3-1.5z'/%3E%3Cpath fill='%23fff' opacity='.2' d='M30.3 39.7s-10.6-4-11-14.2l-.2-5.5c-4.2 19.1 9.2 20.2 11.2 19.7z'/%3E%3Cpath fill='%239f235e' d='M38.3 31.5s-1.9-5.9-5.8-8.3c-3-1.8-8.2-3.4-8.2-3.4-1.5-.8 4.8-.7 9.4 2.2 4.6 2.9 4.7 7.9 4.7 7.9.1.1 0 1.7-.1 1.6z'/%3E%3C/g%3E%3Cg%3E%3Cpath fill='%23e40986' d='M8.2.7c-1 1.5-1.1 5.5-1.1 6.6 0 .1.1.2.2.1.4-.2 1.3-.5 2-.4 1.5.2 2.2.8 1.6 2.7-.4 1.2-1.1 1.7-2.1 2-1.6.5-2.4 0-2.5-1.7 0-.5.3-1.4.4-1.9 0-.1 0-.3-.2-.3-1.2-.1-5.3.2-6.3 1-.7 11.8 15.3 13.4 19 11.4C21.1 15.3 21.3.4 8.2.7z'/%3E%3Cpath fill='%23ee7e26' d='M9.4 7.7c-.5.1-1.4.3-1.7.8-1.9 2-.7 3.3 1.8 3.3.3.2 1.3-.3 1.4-1.5.1-.6-.5-2.8-1.5-2.6z'/%3E%3Cellipse transform='rotate(-45.001 8.564 9.613)' fill='%23f7ec17' cx='8.6' cy='9.6' rx='1.9' ry='1'/%3E%3Cpath fill='%234d887c' d='M9.5 11.8c2.8 1.8 6 5.4 6 5.4l.8-.9s-4.6-3.7-5.5-6c-.1 1.2-.8 1.1-1.3 1.5z'/%3E%3Cpath fill='%23fff' opacity='.2' d='M8.2.7s10.6 4 11 14.2l.2 5.5C23.5 1.3 10.1.3 8.2.7z'/%3E%3Cpath fill='%239f235e' d='M.1 8.9s1.9 5.9 5.8 8.3c3 1.8 8.2 3.4 8.2 3.4 1.5.8-4.8.7-9.4-2.2C.1 15.5 0 10.5 0 10.5c0-.1.1-1.6.1-1.6z'/%3E%3C/g%3E%3Cg id='Layer_2'%3E%3Ccircle cx='19.4' cy='20.1' r='5.8' fill='%23ae4b84'/%3E%3Ccircle fill='%23ee7e26' cx='19.4' cy='20.1' r='5.2'/%3E%3Ccircle fill='%23f7ec17' cx='19.4' cy='20' r='4.5'/%3E%3Cpath d='M20.1 15.6c-.3 0 2 9.1.1 9v.1c6.6-2.3 3.2-9.4-.1-9.1z' opacity='.35' fill='%23ee7e29'/%3E%3C/g%3E%3C/svg%3E"), auto;
}

.dawn {
    background: linear-gradient(#123352, #1d5372, #3885a2);
  }

  .morning {
    background: linear-gradient(#8dcdba, #eee07a, #f0eebc);
    color: #363c80;
  }

  .afternoon-1 {
    background: linear-gradient(#f0eebc, #e6756f, #ee8049);
    color: #fff;
  }

  .afternoon-2 {
    background: linear-gradient(#ee8049, #facf62, #e6756f);
    color: #fff;
  }

  .evening-1 {
    background: linear-gradient(#facf62, #e6756f, #884186);
    color: #fff;
  }

  .evening-2 {
    background: linear-gradient(#291c6b, #4a257d, #884186);
    color: #9eaf81;
  }

  .night-1 {
    background: linear-gradient(#884186, #111642, #011548);
  }

  .night-2 {
    background: linear-gradient(#111642, #1d5372, #123352);
  }

  html, body {
    margin: 0;
    font-family: var(--font);
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    background-color: var(--background);
    min-height: 100vh;
  }
  
  input, textarea, button {
    font-family: var(--font);
  }

  a {
    /* cursor: default; */
    text-decoration: none;
  }

  .react-transform-component{
    width: 100vw  !important;
    height: 100vh !important;
    background-color: var(--background);
  }
  .react-transform-element{
    background-color: var(--background);
    cursor:grab;
    display:inline-flex;
  }
  .react-transform-element:active{
    cursor:grabbing;
  }
`
