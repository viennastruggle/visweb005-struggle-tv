import { Auth0Provider } from "@auth0/auth0-react"
import * as React from "react"
import { QueryClient, QueryClientProvider } from "react-query"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import { ToastProvider } from "react-toast-notifications"
import styled from "styled-components"
import Profile from "./components/Account/UserProfile"
import { Enter } from "./pages/Enter/Enter"
import { Home } from "./pages/Home/Home"
import { Session } from "./pages/Session/Session"

const queryClient = new QueryClient()

const AppContainer = styled.div`
  /* cursor: default; */
`

function App() {
  return (
    <AppContainer>
      <ToastProvider autoDismissTimeout={20000}>
        <Auth0Provider
          domain="viennastruggle.eu.auth0.com"
          clientId="fdZPOztvHPX8rf0TSBMckw7xZdYLsDLK"
          redirectUri={window.location.origin}
        >
          <QueryClientProvider client={queryClient}>
            <Router>
              <Switch>
                <Route path="/enter/:id">
                  <Enter />
                </Route>
                <Route path="/profile">
                  <Profile />
                </Route>

                <Route path="/session/:id">
                  {/* TODO: redirect to "/enter" if this the first time the user in this conference */}
                  <Session />
                </Route>

                <Route path="/">
                  <Home />
                </Route>
              </Switch>
            </Router>
          </QueryClientProvider>
        </Auth0Provider>
      </ToastProvider>
    </AppContainer>
  )
}

export default App
