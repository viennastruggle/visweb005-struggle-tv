import * as React from "react"
import { useParams } from "react-router-dom"
import styled from "styled-components"
import { BigHeadline } from "../../components/common/BigHeadline"
import { ErrorHandler } from "../../components/common/Info/ErrorHandler"
import { Info } from "../../components/common/Info/Info"
import { SubHeadline } from "../../components/common/SubHeadline"
import { Footer } from "../../components/Footer/Footer"
import { JoinButton } from "../../components/Footer/JoinButton/JoinButton"
import { MuteButton } from "../../components/Footer/MuteButton/MuteButton"
import { Localuser } from "../../components/Localuser/Localuser"
import { PanWrapper } from "../../components/PanWrapper/PanWrapper"
import { Room } from "../../components/Room/Room"
import { useConferenceStore } from "../../store/ConferenceStore"
import { LocalStoreLogic } from "../../store/LocalStoreLogic"
import { ReactComponent as Wave } from "./../../assets/wave.svg"
import { UserDragContainer } from "./../../components/Localuser/LocalUserContainer"

const BigHeadContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

const CenterContainer = styled.div`
  padding-top: 8rem;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  z-index: 1;
`

export const Enter = () => {
  const { id } = useParams() //get Id from url, should error check here I guess
  const setConferenceName = useConferenceStore(
    (store) => store.setConferenceName,
  )

  React.useEffect(() => {
    setConferenceName(id)
  }, [id, setConferenceName])

  return (
    <React.Fragment>
      <Info>
        Welcome to our Prototype
        <br />
        Please use <b>Safari</b> or <b>Chrome</b> for now for a stable
        Experience
      </Info>
      <LocalStoreLogic />
      <PanWrapper>
        <Room>
          <UserDragContainer>
            <Localuser />
          </UserDragContainer>
        </Room>
      </PanWrapper>
      <ErrorHandler />
      <CenterContainer id="centerContainer">
        <BigHeadContainer>
          <Wave />
          <BigHeadline>Welcome to Chatmosphere</BigHeadline>
        </BigHeadContainer>
        <SubHeadline>The Open Source Videochat for Cozy Talks</SubHeadline>
      </CenterContainer>
      <Footer>
        <MuteButton />
        <JoinButton />
        {/* <VideoButton /> */}
      </Footer>
    </React.Fragment>
  )
}
