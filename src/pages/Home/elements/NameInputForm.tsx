import * as React from 'react'
import styled from 'styled-components'

const Form = styled.form`
  /* width: 340px; */
  margin: auto;
  text-align: left;
`
const Fieldset = styled.fieldset`
  border: none;
  padding: 0;
  margin: 10px 0 0 0;
  display: flex;
  flex-direction: row;
`

const Label = styled.label`
  font-size: ${props => props.theme.fontSize.small};
  color: black;
  font-weight: bold;
`

const InputField = styled.input`
	height: 50px;
	background: #f5f5f5;
  color: black;
	border: 1px solid black;
  font-size: 1rem;
	box-sizing: border-box;
	border-radius: .25rem 0 0 .25rem;
  border: none;
  padding-left: 20px;
  /* width: 100%; */
  &:hover {
    background: white;
  }
  &:focus {
    background: #f5f5f5;
    outline: none;
    font-size:1rem;
  }
  &::placeholder {
    font-size: 1rem;
  }
  &:disabled {
    opacity: .5;
  }
`

const JoinButton = styled.input`
  height: 50px;
  background: var(--third);
  width: 111px;
  color: var(--text);
  font-size: 1rem;
  font-weight: bold;
  border: none;
  cursor: pointer;
  border-radius: 0 .25rem .25rem 0;

  &:hover {
    background-color: var(--second);
    color: white;
  }
`

export const NameInputForm = ({ defaultSessionName, onSubmit, handleChange }) => {
	return (
		<Form onSubmit={onSubmit}>
			<Label htmlFor="sessionName">Set Session Name</Label>
			<Fieldset>
				<InputField
					name="sessionName"
					type="text"
          placeholder={defaultSessionName}
					onChange={handleChange}
          id="sessionName"
          disabled={process.env.REACT_APP_DEMO_SESSION ? true : false}
				/>
				<JoinButton name="joinButton" type="submit" value="Join" />
			</Fieldset>
        {/* <InfoBubble>Only Demo Session Available</InfoBubble> */}
		</Form>
	)
}
