import { useAuth0 } from "@auth0/auth0-react"
import React from "react"
import styled from "styled-components"
import LoginButton from "../../components/Account/LoginButton"
import { Info } from "../../components/common/Info/Info"
import { SubHeadline } from "../../components/common/SubHeadline"
import SessionsList from "../../components/Flags/Presets/SessionsList"
import { SocialIcons } from "../../components/Footer/SocialIcons"
import Navigation from "../../components/Header/Navigation"
import { NameInputContainer } from "./elements/NameInputContainer"

const Header = styled.div`
  font-size: 2rem;
  padding: 3rem;
  border-bottom: 1px solid;
  background: #ffff00;
  padding-bottom: 4rem;
  min-height: 90vh;
  @media screen and (max-width: 800px){
    padding: 1rem;
  }
  h1 {
    margin-top: 0;
    margin-bottom: 1rem;
  }
`

const CenterContainer = styled.div`
  /* padding: 2rem; */
`

const FormContainer = styled.section`
  background: var(--background);
  padding: 3rem;
  min-height: 100vh;
  @media screen and (max-width: 800px){
    padding: 1rem;
  }
  > div {
    max-width: 28rem;
    /* margin: 0 auto; */
  }
  button {
    border: 0;
    background: black;
    color: white;
    border-radius: 0.25rem;
    cursor: pointer;
    &:hover {
      background: blue;
    }
  }
  h3 {
    margin-top: 4rem;
    font-size: 4rem;
  }
`

const ShowCase = styled.div`
  border-top: 1px solid var(--background);

  min-height: 100vh;
  & > img {
    width: 100%;
    display: block;
  }
  & > h2 {
    color: var(--background);
    margin-top: 4rem;
    padding: 0 2rem;
    margin-bottom: 4rem;
    font-size: 4rem;
  }
`

const BaseContainer = styled.div`
  background: var(--text);
  color: var(--background);
`

const FooterContainer = styled(BaseContainer)`
  text-align: center;
  padding: 3rem;
  line-height: 1.5rem;
  @media screen and (max-width: 800px){
    padding: 1rem;
  }
  ul {
    margin: 0;
    list-style: none;
    padding: 0;
    li {
      display: inline;
      padding: 0 0.5rem;
      a {
        color: var(--background);
        &:hover {
          color: var(--color);
        }
      }
    }
  }
`

export const Home = () => {
  const { isAuthenticated } = useAuth0()
  return (
    <>
      <Info>
        Welcome to our Prototype
        <br />
        Please use <b>Safari</b> or <b>Chrome</b> for now for a stable
        Experience
      </Info>
      <Navigation />
      <CenterContainer>
        <Header>
          <h1>
            Welcome to
            <br />
            Struggle Sessions 🤜🏿 🤛
          </h1>
          <SubHeadline>
            Video calls for people who care about each others struggles &amp;
            culture.
          </SubHeadline>
        </Header>
        <FormContainer>
          <div>
            <h3>Start your own!</h3>
            <NameInputContainer />
            {!isAuthenticated && (
              <p>
                You are already a member of Vienna Struggle? <br />
                <LoginButton /> to let people who you are.
              </p>
            )}
          </div>
        </FormContainer>
      </CenterContainer>
      <BaseContainer>
        <SessionsList />
        <ShowCase>
          <h2>Chat about topics that matter to you</h2>
          <img
            src="https://res.cloudinary.com/ddqo2y6zh/image/upload/q_81/v1624461542/struggle/fourthgarden/visweb005-jve-screenshot.jpg"
            alt="Show case"
          />
        </ShowCase>
      </BaseContainer>
      <FooterContainer>
        <SocialIcons />
        <small>&copy; 2021 Vienna Struggle</small>
      <div>
        <ul>
          <li>
            <a href="/team">Team</a>
          </li>
          <li>
            <a href="https://www.viennastruggle.com/statutes">NGO Statutes</a>
          </li>
          <li>
            <a href="https://www.viennastruggle.com/page/terms">Terms</a>
          </li>
          <li>
            <a href="https://www.viennastruggle.com/page/privacy-policy">Privacy Policy</a>
          </li>
        </ul>
      </div>
      </FooterContainer>
    </>
  )
}
