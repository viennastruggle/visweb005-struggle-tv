import { BrowserView, MobileView } from "react-device-detect"
import styled from "styled-components"
import { ErrorHandler } from "../../components/common/Info/ErrorHandler"
import { SessionInfo } from "../../components/common/Info/SessionInfo"
import SessionsList from "../../components/Flags/Presets/SessionsList"
import { Footer } from "../../components/Footer/Footer"
import { JoinButton } from "../../components/Footer/JoinButton/JoinButton"
import { MuteButton } from "../../components/Footer/MuteButton/MuteButton"
import { Header } from "../../components/Header/Header"
import JitsiConnection from "../../components/JitsiConnection/JitsiConnection"
import { Localuser } from "../../components/Localuser/Localuser"
import { UserDragContainer } from "../../components/Localuser/LocalUserContainer"
import { PanWrapper } from "../../components/PanWrapper/PanWrapper"
import { Room } from "../../components/Room/Room"
import { Users } from "../../components/User/Users"
import { useConferenceStore } from "../../store/ConferenceStore"
import { LocalStoreLogic } from "../../store/LocalStoreLogic"

const Container = styled.div`
  /* #DragElement {
      position: absolute;
      top: 0;
      left: 0;
    } */
  transition: all 1sec;
`

const MobileNotification = styled.section`
  margin: 0 1rem;
  box-shadow: var(--shadow-blur);
  padding: 1rem;
  display: flex;
  flex-direction: column;
  height: calc(100vh - 2rem);
  overflow: auto;
  & > p {
    background: yellow;
    padding: 0.5rem;
  }
`

export const PluginSession = () => {
  const { conferenceName } = useConferenceStore()
  return (
    <Container>
      <MobileView>
        <MobileNotification>
          <h1>Struggle.tv</h1>
          {/* <h2>
            {conferenceName}
            </h2> */}
          <p>
            Disabled on mobile devices. Please use your
            computer for the full experience for now.
          </p>
          <SessionsList />
        </MobileNotification>
      </MobileView>
      <BrowserView>
        <ErrorHandler />
        {conferenceName === "fourthgarden" && (
          <SessionInfo>
            Welcome to the <b>Fourth Garden</b> of the digital Open Studios of
            Jan Van Eyck Academy 2021.
            <p>
              Explore the artists growing in the garden by dragging yourself
              around and zooming in and out. Add your name underneath your
              bubble so we can meet and chat together. Check our program, the
              unexpected can happen...
            </p>
          </SessionInfo>
        )}
        <Header>Struggle.tv</Header>
        <JitsiConnection />
        <LocalStoreLogic />
        <PanWrapper>
          <Room>
            {/* <FlagLayer /> */}
            <Users />
            <UserDragContainer>
              <Localuser audioRadius />
            </UserDragContainer>
          </Room>
        </PanWrapper>

        {/* UI */}
        {/* {conferenceName === "fourthgarden" && (
            <>
              <Chat />
              <BroadcastMessage />
              <MobileView>
                <ExhibitorList />
              </MobileView>
              <Clock />
            </>
          )} */}

        <Footer>
          <JoinButton joined={true} />
          <MuteButton />
        </Footer>
      </BrowserView>
    </Container>
  )
}
