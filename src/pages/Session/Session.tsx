import { BrowserView, MobileView } from "react-device-detect"
import styled from "styled-components"
import { default as AccountMenu } from "../../components/Account/AccountMenu"
import CreateYourOwn from "../../components/Advertisment/CreateYourOwn"
import BroadcastMessage from "../../components/Chats/BroadcastMessage"
import Chat from "../../components/Chats/Chat"
import { ChatPanel } from "../../components/Chats/ChatPanel"
import { ErrorHandler } from "../../components/common/Info/ErrorHandler"
import { SessionInfo } from "../../components/common/Info/SessionInfo"
import FlagLayer from "../../components/Flags/FlagLayer"
import ExhibitorList from "../../components/Flags/Presets/ExhibitorList"
import FocusGroupList from "../../components/Flags/Presets/FocusGroupList"
import { GardenHeader } from "../../components/Flags/Presets/GardenLogo"
import { Footer } from "../../components/Footer/Footer"
import { JoinButton } from "../../components/Footer/JoinButton/JoinButton"
import { MuteButton } from "../../components/Footer/MuteButton/MuteButton"
import { VideoButton } from "../../components/Footer/VideoButton/VideoButton"
import { Header } from "../../components/Header/Header"
import ViennaStruggleLogo from "../../components/Header/ViennaStruggleLogo"
import JitsiConnection from "../../components/JitsiConnection/JitsiConnection"
import { Localuser } from "../../components/Localuser/Localuser"
import { UserDragContainer } from "../../components/Localuser/LocalUserContainer"
import { LocationPanel } from "../../components/LocationPanel/LocationPanel"
import { PanWrapper } from "../../components/PanWrapper/PanWrapper"
import { Room } from "../../components/Room/Room"
import Sidebar from "../../components/Sidebar/Sidebar"
import { Users } from "../../components/User/Users"
import { useConferenceStore } from "../../store/ConferenceStore"
import { LocalStoreLogic } from "../../store/LocalStoreLogic"

const Container = styled.div`
  #DragElement {
    z-index: 2;
  }
  transition: all 1sec;
`

const MobileNotification = styled.section`
  /* margin: 0 1rem; */
  padding: 1rem;
  display: flex;
  flex-direction: column;
  height: calc(100vh - 2rem);
  overflow: auto;
  align-items: center;
  & > svg {
    flex: 1;
  }
  & > p {
    background: yellow;
    padding: 0.5rem;
    /* flex: 1; */
  }
`

export const Session = () => {
  const { conferenceName } = useConferenceStore()
  return (
    <Container>
      <MobileView>
        <MobileNotification>
          <ViennaStruggleLogo />
          {/* <h1>Struggle.tv</h1> */}
          <p>
            Disabled on mobile devices. Please use your computer for the full
            experience for now.
          </p>
          <JoinButton joined={true} />
        </MobileNotification>
      </MobileView>
      <ErrorHandler />
      <BrowserView>
        {conferenceName === "fourthgarden" ? (
          <>
            <SessionInfo>
              <p>
                Welcome! Explore the artists growing in the Fourth Garden by
                dragging yourself around and zooming in and out.
              </p>
              <p>
                Add your name underneath your bubble so we can meet and chat
                together.
              </p>
              <p>Check our program, the unexpected can happen...</p>
            </SessionInfo>
            <GardenHeader />
          </>
        ) : (
          <Header>Struggle.tv</Header>
        )}
        <JitsiConnection />
        <LocalStoreLogic />
        <PanWrapper>
          <Room>
            <UserDragContainer>
              <Localuser audioRadius />
            </UserDragContainer>
            <FlagLayer />
            <Users />
          </Room>
        </PanWrapper>
        {/* UI */}
        {conferenceName === "fourthgarden" && (
          <>
            <Chat />
            <BroadcastMessage />
          </>
        )}
        <Sidebar>
          <AccountMenu />
          {conferenceName === "fourthgarden" && <FocusGroupList />}
          <ExhibitorList />
          <ChatPanel />
          <LocationPanel />
        </Sidebar>
        <Footer>
          <JoinButton joined={true} />
          <VideoButton />
          <MuteButton />
        </Footer>
        <CreateYourOwn />
      </BrowserView>
    </Container>
  )
}
